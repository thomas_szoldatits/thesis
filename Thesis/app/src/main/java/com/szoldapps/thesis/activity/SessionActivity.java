package com.szoldapps.thesis.activity;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.szoldapps.tealib.TeaLib;
import com.szoldapps.thesis.helper.LinkingHelper;
import com.szoldapps.thesis.R;

public class SessionActivity extends ActionBarActivity  {

    private static final String LOGTAG = SessionActivity.class.getSimpleName();
    public static final String EXTRA_COMING_FROM_ANOTHER_ACTIVITY = LOGTAG + ".coming_from_another_activity";
    private EditText mEtName;
    private Button mBtnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session);

        mEtName = (EditText) findViewById(R.id.session_tv_name);
        mBtnNext = (Button) findViewById(R.id.session_btn_next);

        boolean comingFromAnotherActivity = getIntent().getBooleanExtra(EXTRA_COMING_FROM_ANOTHER_ACTIVITY, false);
        if(comingFromAnotherActivity) {
            mEtName.setText(TeaLib.getInstance().getSessionName());
        }

        addListeners();
    }

    private void addListeners() {
        mEtName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                //noinspection SimplifiableIfStatement
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    return goToRoundActivity();
                }
                return false;
            }
        });

        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToRoundActivity();
            }
        });
    }

    private boolean goToRoundActivity() {
        String name = mEtName.getText().toString();
        if (name.equals("")) {
            mEtName.setError(getString(R.string.please_enter_your_name));
            mEtName.requestFocus();
            return false;
        } else {
            TeaLib.getInstance().startSession(mEtName.getText().toString());
            LinkingHelper.goToRoundActivity(this);
            return true;
        }
    }

}


//                        DEBUG ---------------
//                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
//                        InputMethodSubtype ims = imm.getCurrentInputMethodSubtype();
//                        String localeString = ims.getLocale();
//                        Locale locale = new Locale(localeString);
//                        String currentLanguage = locale.getDisplayLanguage();
//
//                        Log.e(LOGTAG, "displayName = " + locale.getDisplayName());
//                        Log.e(LOGTAG, "curLang = " + currentLanguage);
//                        DEBUG ---------------
