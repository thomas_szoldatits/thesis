package com.szoldapps.tealib;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.provider.Settings;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.szoldapps.tealib.csv.CSVFile;
import com.szoldapps.tealib.csv.CSVWriter;
import com.szoldapps.tealib.listener.TeaLibListener;
import com.szoldapps.tealib.model.Phrase;
import com.szoldapps.tealib.model.Round;
import com.szoldapps.tealib.model.RoundSettings;
import com.szoldapps.tealib.model.Session;
import com.szoldapps.tealib.stats.Metrics;
import com.szoldapps.tealib.stats.PhraseStats;
import com.szoldapps.tealib.stats.RoundStats;
import com.szoldapps.tealib.stats.SessionStats;
import com.szoldapps.tealib.time.StopWatch;
import com.szoldapps.tealib.widget.TeaEditText;
import com.szoldapps.thesislib.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * TeaLib
 * Created by thomasszoldatits on 13.12.14.
 */
public class TeaLib {

    private static final String LOGTAG = TeaLib.class.getSimpleName();
    // Keyboards
    public static final String ANDROID_DEFAULT_KEYBOARD = "Android Default Keyboard";
    public static final String SWIFTKEY_KEYBOARD = "SwiftKey";
    public static final String DIOPEN_KEYBOARD = "DioPen Script";
    public static final String GOOGLE_VOICE_INPUT = "Google Voice Input";
    public static final String GO_KEYBOARD = "GO Keyboard";
    public static final String SWYPE_KEYBOARD = "Swype";

    private EditText mEtPresented;
    private TeaEditText mTeaEtTranscribed;

    // StopWatch
    private StopWatch mStopWatch;
    private static TeaLib instance;
    private Session mSession;
    private Round mRound;
    private TeaLibListener mListener;
    private boolean mIgnoreOneBackspace = false;
    private String mDeviceName;
    private Context mContext;
    private int mPhraseCount = -1;
    private int mPhraseCounter = 1;
    private RoundSettings mRoundSettings;

    protected TeaLib() {
        // Exists only to defeat instantiation.
    }

    public static TeaLib getInstance() {
        if (instance == null) {
            instance = new TeaLib();
        }
        return instance;
    }

    public void startSession(String sessionName) {
        Log.d(LOGTAG, "Session '" + sessionName + "' started");
        mSession = new Session(sessionName, getDeviceName());
    }

    public void startSessionWithDefaultName(Context context) {
        startSession(context.getString(R.string.default_name));
    }

    public boolean isSessionStarted() {
        return mSession != null;
    }

    public int prepareNewRound(RoundSettings roundSettings, Context context) throws IOException {
        if (mSession == null) {
            startSessionWithDefaultName(context);
        }
        mPhraseCount = roundSettings.getPhraseCount();
        mPhraseCounter = 1;
        mRoundSettings = roundSettings;
        return mSession.prepareNewRound(roundSettings, context);
    }

    public int prepareNewRoundWithSameSettings(Context context) throws IOException {
        return prepareNewRound(mRoundSettings, context);
    }


    /**
     * Actually starts a new round. {@link #prepareNewRound(RoundSettings, Context)}  should
     * be called prior to this.
     *
     * @param roundNr       Round number
     * @param etPresented   EditText with presented Text
     * @param etTranscribed TeaEditText with transcribed Text
     * @param tvTimer       TextView that displays the timer
     * @param listener      TeaLibListener
     * @param context       Application context
     */
    public void startRound(int roundNr, EditText etPresented, TeaEditText etTranscribed,
                           TextView tvTimer, TeaLibListener listener, Context context) {
        this.mEtPresented = etPresented;
        this.mTeaEtTranscribed = etTranscribed;
        // Check whether to show suggestions on the keyboard or not
        if (mRoundSettings.isShowSuggestions()) {
            mTeaEtTranscribed.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        } else {    // TYPE_CLASS_TEXT is needed, otherwise return btn not displayed
            mTeaEtTranscribed.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_MULTI_LINE |
                    InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        }
        this.mStopWatch = new StopWatch(tvTimer);
        this.mRound = mSession.getRound(roundNr); // TODO: What happens if roundNr does not exist
        this.mListener = listener;
        this.mContext = context;
        initEts();
    }

    private void initEts() {
        // fill mEtPresented with first Phrase
        fillPresentedWithNextPhrase(true);

        // init Listener on mTeaEtTranscribed
        mTeaEtTranscribed.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            int prevStart = 0;
            int prevBefore = 0;
            int prevCount = 0;

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (mStopWatch.isPaused()) {
                    mListener.onPlayEvent();
                    mStopWatch.start();
                }
                if (mIgnoreOneBackspace) {
                    mIgnoreOneBackspace = false;
                    return;
                }

                long curTimeInMs = mStopWatch.getCurrentMs();
                int actualCount = start + count;

                if (actualCount == prevCount + 1) {
                    // ADD ONE CHAR
                    log(s.toString(), start, before, count, actualCount, s.charAt(s.length() - 1), "ADD 1");

                    // s.charAt(s.length() - 1) is guaranteed b/c actualCount == prevCount + 1
                    char lastChar = s.charAt(s.length() - 1);
                    switch (lastChar) {
                        case '\n':
                            Log.e(LOGTAG, "<RETURN> recognized. called next phrase!");
                            String transcribed = s.toString();
                            // Substring to remove the \n at the end
                            mRound.getCurrentPhrase().setTranscribed(
                                    transcribed.substring(0, transcribed.length() - 1));
                            fillPresentedWithNextPhrase(false);
                            // reset member vars for next phrase
                            prevStart = prevBefore = prevCount = actualCount = 0;
                            break;
                        default:
                            mRound.getCurrentPhrase().addKeystroke(curTimeInMs, lastChar, false);
                    }

                } else if (actualCount > prevCount + 1) {
                    // ADD MULTIPLE CHARS
                    for (int i = (start + before); i < actualCount; i++) {
                        log(s.toString(), start, before, count, actualCount, s.charAt(i),
                                "ADD MULTI (added " + (actualCount - (start + before)) + ")");
                        char curChar = s.charAt(i);
                        switch (curChar) {
                            case '\n':
                                Log.e(LOGTAG, "\nin HERE\n");
                                String transcribed = s.toString();
                                // Substring to remove the \n at the end
                                mRound.getCurrentPhrase().setTranscribed(
                                        transcribed.substring(0, transcribed.length() - 1));
                                fillPresentedWithNextPhrase(false);
                                // reset member vars for next phrase
                                //noinspection UnusedAssignment
                                prevStart = prevBefore = prevCount = actualCount = 0;
                                return;
                            default:
                                mRound.getCurrentPhrase().addKeystroke(curTimeInMs, curChar, true);
                        }
                    }
                } else if (actualCount == prevCount - 1) {
                    // DELETE ONE CHAR
                    log(s.toString(), start, before, count, actualCount, ' ', "DELETE 1");

                    mRound.getCurrentPhrase().addKeystroke(curTimeInMs, Character.toChars(8)[0], false);

                } else if (actualCount < prevCount - 1) {
                    // DELETE MULTIPLE CHARS
                    for (int i = before; i > count; i--) {
                        log(s.toString(), start, before, count, actualCount, ' ',
                                "DELETE MULTI (removing " + (before - count) + ")");
                        mRound.getCurrentPhrase().addKeystroke(curTimeInMs, Character.toChars(8)[0], true);
                    }
                } else {
                    // THESE OCCURRENCES CAN BE IGNORED
                    // e.g. if you type pop, this listener receives multiple different phrases like popular etc.
                    //log(s.toString(), start, before, actualCount, "**SOMETHING ELSE**");
                    return;
                }

                // on RETURN set prevs = 0

                // setValues for next iteration
                prevStart = start;
                prevBefore = before;
                prevCount = actualCount;
            }

            /**
             * Just here for debugging purposes.
             * @param s .
             * @param start .
             * @param before    .
             * @param count .
             * @param actualCount   .
             * @param lastChar .
             * @param additionalStr .
             */
            private void log(String s, int start, int before, int count, int actualCount,
                             char lastChar, String additionalStr) {
                s = s.replace("\n", "<RETURN>");
                String str = "start=" + start + " | before=" + before + " | count=" + count + " | actualCount=" +
                        actualCount + " | lastChar = " + lastChar + " | s=" + s + " (" + mStopWatch.getCurrentMs() +
                        ") " + additionalStr;
                Log.d(LOGTAG, str);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    /**
     * Simply appends a return char to {@link this.mTeaEtTranscribed} and therefore provokes the listener to go to
     * the next phrase.
     */
    public void goToNextPhrase() {
        mTeaEtTranscribed.append("\n");
    }

    private void fillPresentedWithNextPhrase(boolean firstCall) {
        if (mRound != null) {
            Phrase curPhrase = mRound.getCurrentPhrase();
            // Write Pause and EndTime to the finished Phrase
            if (curPhrase != null) {
                // set Phrase PauseTime
                curPhrase.setPauseTime(mStopWatch.getPauseTimeAndReset());
                // set Phrase EndTime
                curPhrase.setEndTime(mStopWatch.getCurrentMs());

                // Checks current Keyboard if it is the SWIFTKEY Keyboard
                // if yes -> if last char is space -> delete it
                if(getKeyboardName(mContext).contains(SWIFTKEY_KEYBOARD) ||
                        getKeyboardName(mContext).contains(GOOGLE_VOICE_INPUT)) {
                    curPhrase.deleteLastKeystrokeIfSpaceChar();
                }
            }
            // Handle the next Phrase
            Phrase nextPhrase = mRound.getNextPhrase();
            if (nextPhrase != null) {
                // set Phrase EndTime
                nextPhrase.setStartTime(mStopWatch.getCurrentMs());

                // Change Presented if wanted
                if (mRoundSettings.isLowerCaseOnly() || mRoundSettings.isAlphaNumOnly() || mRoundSettings.isAutoCapFirst()) {
                    String nextP = nextPhrase.getPresented();
                    // Auto Cap First Letter
                    nextP = mRoundSettings.isAutoCapFirst() ? capitalize(nextP) : nextP;
                    // Remove uppercase chars if wanted
                    nextP = mRoundSettings.isLowerCaseOnly() ? nextP.toLowerCase() : nextP;
                    // Remove all non-alphanumeric chars if wanted
                    nextP = mRoundSettings.isAlphaNumOnly() ? nextP.replaceAll("[^a-zA-Z0-9öäüß ]", "") : nextP;
                    // Actually change presented in model
                    nextPhrase.setPresented(nextP);
                }
                // fill Presented Et with next Phrase
                mEtPresented.setText(nextPhrase.getPresented());
                mListener.onNextPhraseEvent(mPhraseCounter++, mPhraseCount);
                // clear Transcribed (therefore a backspace is happening that is no user input)
                mIgnoreOneBackspace = !firstCall;
                mTeaEtTranscribed.setText("");
            } else {
                mStopWatch.stop();


                if (mRoundSettings.isTrainingRound()) {
                    mSession.deleteLastRound();
                    mListener.trainingIsOver();
                } else {
                    // Round is over, get start and stop time from stopwatch and keyboard
                    Log.d(LOGTAG, "start:" + mStopWatch.getStartTime() + " | end:" + mStopWatch.getStopTime());
                    mRound.setStartTime(mStopWatch.getStartTime());
                    mRound.setEndTime(mStopWatch.getStopTime());
                    mRound.getRoundSettings().setKeyboard(getKeyboardName(mContext));

                    // clear Transcribed (therefore a backspace is happening that is no user input)
                    mListener.noMorePhrasesLeft();
                }
                mIgnoreOneBackspace = !firstCall;
                mTeaEtTranscribed.setText("");
            }
        }
    }

    /**
     * Opens the Android Input Method Picker.
     *
     * @param context Application context
     */
    public static void showInputMethodPicker(Context context) {
        InputMethodManager imeManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imeManager != null) {
            imeManager.showInputMethodPicker();
        } else {
            Toast.makeText(context, "NOT POSSIBLE", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Delivers the device name incl. manufacturer if available
     *
     * @return device name
     */
    public String getDeviceName() {
        if (mDeviceName != null) {
            return mDeviceName;
        }
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return mDeviceName = capitalize(model);
        } else {
            return mDeviceName = capitalize(manufacturer) + " " + model;
        }
    }

    /**
     * Delivers shortened Keyboard name.
     *
     * @param context Application context
     * @return Keyboard name
     */
    public String getKeyboardName(Context context) {
        String currentLanguage = "n/a";
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        InputMethodSubtype ims = imm.getCurrentInputMethodSubtype();
        if (ims != null) {
            String localeString = ims.getLocale();
            Locale locale = new Locale(localeString);
            currentLanguage = locale.getDisplayLanguage();
        }

        String keyboard = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.DEFAULT_INPUT_METHOD);
        Log.i(LOGTAG, keyboard);
        switch (keyboard) {
            case "com.google.android.inputmethod.latin/com.android.inputmethod.latin.LatinIME":
                keyboard = ANDROID_DEFAULT_KEYBOARD;
                break;
            case "com.touchtype.swiftkey/com.touchtype.KeyboardService":
                keyboard = SWIFTKEY_KEYBOARD;
                break;
            case "com.diotek.diopen.script/com.diotek.hwr.ims.HwrInputMethodService":
                keyboard = DIOPEN_KEYBOARD;
                break;
            case "com.google.android.googlequicksearchbox/com.google.android.voicesearch.ime" +
                    ".VoiceInputMethodService":
                keyboard = GOOGLE_VOICE_INPUT;
                break;
            case "com.jb.emoji.gokeyboard/com.jb.gokeyboard.GoKeyboard":
                keyboard = GO_KEYBOARD;
                break;
            case "com.nuance.swype.dtc/com.nuance.swype.input.IME":
                keyboard = SWYPE_KEYBOARD;
                break;
        }
        if (currentLanguage.length() == 0) {
            currentLanguage = "n/a";
        }
        return keyboard + " (" + currentLanguage + ")";
    }

    /**
     * Returns s with first letter capitalized.
     *
     * @param s String to be capitalized
     * @return Capitalized string
     */
    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }


    /**
     * Returns the contents of the directory assets/phrasesets in both, TeaLib and the calling App.
     *
     * @param activity Activity
     * @return List of Phrasessets
     * @throws IOException
     */
    public List<String> getAssetsPhrasesetsFileList(Activity activity) throws IOException {
        String dirFrom = "phrasesets";
        Resources res = activity.getResources(); //if you are in an activity
        AssetManager am = res.getAssets();
        String fileList[] = am.list(dirFrom);
        List<String> list = new ArrayList<String>();

        for (String fName : fileList) {
            if (fName.contains(".txt")) {
                list.add(fName.substring(0, fName.length() - 4));
            }
        }
        return list;
    }

    public SessionStats getSessionStats(Activity activity) {
        if (mSession == null) {
            return null;
        }

        int pChars = 0, tChars = 0, msd = 0, bsCount = 0, c = 0, inf = 0, f = 0, iF = 0;
        double msdEr = 0, wpm = 0, inputTime = 0, pauseTime = 0, totalTime = 0, kspc = 0, cer = 0, uer = 0, ter = 0,
                corrEff = 0, partCon = 0, utilBandwidth = 0, wastedBandwidth = 0;

        SessionStats sessionStats = new SessionStats(
                mSession.getSessionName(), getDeviceName(), getScreenSizeString(activity));

        List<RoundStats> roundStatList = new ArrayList<>();
        for (Round r : mSession.getRounds()) {
            RoundStats rs = new RoundStats(r.getRoundNr() + 1, r.getStartTime(),
                    r.getEndTime(), r.getRoundSettings());
            List<PhraseStats> phraseStatList = new ArrayList<>();
            for (Phrase p : r.getPhrases()) {
                PhraseStats ps = new PhraseStats(p.getPresented(), p.getTranscribed(), p.getKeystrokes());
                Metrics m = new Metrics();
                // tempValues
                int tempInt;
                double tempDouble;
                // pChars
                tempInt = p.getPresented().length();
                m.setpChars(tempInt);
                pChars += tempInt;
                // tChars
                tempInt = p.getTranscribed().length();
                m.settChars(tempInt);
                tChars += tempInt;
                // input time
                tempDouble = p.getInputTimeInS();
                m.setInputTime(tempDouble);
                inputTime += tempDouble;
                // pause time
                tempDouble = p.getPauseTimeInS();
                m.setPauseTime(tempDouble);
                pauseTime += tempDouble;
                // total time
                tempDouble = p.getTotalTimeInS();
                m.setTotalTime(tempDouble);
                totalTime += tempDouble;
                // WPM
                tempDouble = p.getWordsPerMinute();
                m.setWpm(tempDouble);
                wpm += tempDouble;
                // MSD
                tempInt = p.getMSD();
                m.setMsd(tempInt);
                msd += tempInt;
                // MSD ER
                tempDouble = p.getMsdEr();
                m.setMsdEr(tempDouble);
                msdEr += tempDouble;
                // BS count
                tempInt = p.getBackspaceCount();
                m.setBsCount(tempInt);
                bsCount += tempInt;
                // KSPC
                tempDouble = p.getKeystrokesPerCharacter();
                m.setKspc(tempDouble);
                kspc += tempDouble;
                // C
                tempInt = p.getCorrect();
                m.setC(tempInt);
                c += tempInt;
                // INF
                tempInt = p.getIncorrectNotFixed();
                m.setInf(tempInt);
                inf += tempInt;
                // F
                tempInt = p.getFixed();
                m.setF(tempInt);
                f += tempInt;
                // IF
                tempInt = p.getIncorrectFixed();
                m.setIf(tempInt);
                iF += tempInt;
                // CER
                tempDouble = p.getCorrectedErrorRate();
                m.setCer(tempDouble);
                cer += tempDouble;
                // UER
                tempDouble = p.getUncorrectedErrorRate();
                m.setUer(tempDouble);
                uer += tempDouble;
                // TER
                tempDouble = p.getTotalErrorRate();
                m.setTer(tempDouble);
                ter += tempDouble;
                // Correction Efficiency
                tempDouble = p.getCorrEff();
                m.setCorrEff(tempDouble);
                corrEff += tempDouble;
                // Participant Conscientiousness
                tempDouble = p.getPartCon();
                m.setPartCon(tempDouble);
                partCon += tempDouble;
                // Utilized Bandwidth
                tempDouble = p.getUtilBandwidth();
                m.setUtilBandwidth(tempDouble);
                utilBandwidth += tempDouble;
                // Wasted Bandwidth
                tempDouble = p.getWastedBandwidth();
                m.setWastedBandwidth(tempDouble);
                wastedBandwidth += tempDouble;

                // add Metrics to ps
                ps.setMetrics(m);
                // add PhraseStats to PhraseStatList
                phraseStatList.add(ps);
            }
            // create Round Metrics
            int phraseCount = phraseStatList.size();
            Metrics roundMetrics = new Metrics(
                    pChars, tChars,
                    inputTime, pauseTime, totalTime,
                    wpm / phraseCount, msd, msdEr / phraseCount, bsCount, kspc / phraseCount,
                    c, inf, f, iF,
                    cer / phraseCount, uer / phraseCount, ter / phraseCount,
                    corrEff / phraseCount, partCon / phraseCount,
                    utilBandwidth / phraseCount, wastedBandwidth / phraseCount);
            rs.setMetrics(roundMetrics);
            rs.setPhraseStats(phraseStatList);
            roundStatList.add(rs);
            pChars = tChars = msd = bsCount = c = inf = f = iF = 0;
            msdEr = wpm = inputTime = pauseTime = totalTime = kspc = cer = uer = ter = corrEff = partCon =
                    utilBandwidth = wastedBandwidth = 0;
        }

        sessionStats.setRoundStats(roundStatList);
        for (RoundStats rs : sessionStats.getRoundStats()) {
            Metrics m = rs.getMetrics();
            pChars += m.getpChars();
            tChars += m.gettChars();
            inputTime += m.getInputTime();
            pauseTime += m.getPauseTime();
            totalTime += m.getTotalTime();
            wpm += m.getWpm();
            msd += m.getMsd();
            msdEr += m.getMsdEr();
            bsCount += m.getBsCount();
            kspc += m.getKspc();
            c += m.getC();
            inf += m.getInf();
            f += m.getF();
            iF += m.getIf();
            cer += m.getCer();
            uer += m.getUer();
            ter += m.getTer();
            corrEff += m.getCorrEff();
            partCon += m.getPartCon();
            utilBandwidth += m.getUtilBandwidth();
            wastedBandwidth += m.getWastedBandwidth();
        }
        int roundCount = sessionStats.getRoundStats().size();
        Metrics sessionMetrics = new Metrics(
                pChars, tChars,
                inputTime, pauseTime, totalTime,
                wpm / roundCount, msd, msdEr / roundCount, bsCount, kspc / roundCount,
                c, inf, f, iF,
                cer / roundCount, uer / roundCount, ter / roundCount,
                corrEff / roundCount, partCon / roundCount,
                utilBandwidth / roundCount, wastedBandwidth / roundCount);
        sessionStats.setMetrics(sessionMetrics);
        // Set Session Start & End Time
        if (roundCount > 0) {
            sessionStats.setStartTime(sessionStats.getRoundStats().get(0).getStartTime());
            sessionStats.setEndTime(sessionStats.getRoundStats().get(roundCount - 1).getEndTime());
        }
        return sessionStats;
    }

    /**
     * Writes CSVFile to Download directory.
     *
     * @param sessionStats session statistics
     * @param context      application context
     * @throws IOException if problems with file creation
     */
    public void writeCSVFileToDownloadDir(SessionStats sessionStats, Context context) throws IOException {
        CSVWriter.writeToCSV(getCSVFile(sessionStats, context));
    }

    public CSVFile getCSVFile(SessionStats sessionStats, Context context) {
        return new CSVFile(sessionStats, context);
    }

    /**
     * Toggles stopwatch between play and pause.
     */
    public void toggleStopwatchPlayPause() {
        if (mStopWatch != null) {
            if (mStopWatch.isPaused()) {
                mListener.onPlayEvent();
                mStopWatch.start();
            } else {
                mListener.onPauseEvent();
                mStopWatch.pause();
            }
        }
    }

    /**
     * Simply returns whether Session is paused or not.
     * @return
     */
    public boolean isSessionPaused() {
        if (mStopWatch != null) {
            return mStopWatch.isPaused();
        }
        return false;
    }

    /**
     * Delivers session name
     *
     * @return session name
     */
    public String getSessionName() {
        if (mSession != null) {
            return mSession.getSessionName();
        }
        return "";
    }


    // Needed for Screen Size Calculations
    int mWidthPixels;
    int mHeightPixels;

    /**
     * Returns estimated (depending on the Device) diagonal screen size in inches
     *
     * @param activity needed to get windowManager
     * @return
     */
    public double getDiagonalScreenSize(Activity activity) {
        setRealDeviceSizeInPixels(activity);
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        double x = Math.pow(mWidthPixels / dm.xdpi, 2);
        double y = Math.pow(mHeightPixels / dm.ydpi, 2);
        return Math.sqrt(x + y);
    }

    /**
     * Returns Screen Width in pixels
     *
     * @param activity needed to get windowManager
     * @return
     */
    public int getScreenWidth(Activity activity) {
        setRealDeviceSizeInPixels(activity);
        return mWidthPixels;
    }

    /**
     * Returns Screen Height in pixels
     *
     * @param activity needed to get windowManager
     * @return
     */
    public int getScreenHeight(Activity activity) {
        setRealDeviceSizeInPixels(activity);
        return mHeightPixels;
    }

    private void setRealDeviceSizeInPixels(Activity activity) {
        if (mWidthPixels != 0 && mHeightPixels != 0) {
            return;
        }
        WindowManager windowManager = activity.getWindowManager();
        Display display = windowManager.getDefaultDisplay();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        display.getMetrics(displayMetrics);


        // since SDK_INT = 1;
        mWidthPixels = displayMetrics.widthPixels;
        mHeightPixels = displayMetrics.heightPixels;

        // includes window decorations (statusbar bar/menu bar)
        if (Build.VERSION.SDK_INT >= 14 && Build.VERSION.SDK_INT < 17) {
            try {
                mWidthPixels = (Integer) Display.class.getMethod("getRawWidth").invoke(display);
                mHeightPixels = (Integer) Display.class.getMethod("getRawHeight").invoke(display);
            } catch (Exception ignored) {
            }
        }

        // includes window decorations (statusbar bar/menu bar)
        if (Build.VERSION.SDK_INT >= 17) {
            try {
                Point realSize = new Point();
                Display.class.getMethod("getRealSize", Point.class).invoke(display, realSize);
                mWidthPixels = realSize.x;
                mHeightPixels = realSize.y;
            } catch (Exception ignored) {
            }
        }
    }

    public String getScreenSizeString(Activity activity) {
        return "~ " + Math.round(getDiagonalScreenSize(activity) * 100.0) / 100.0 + " in (" +
                getScreenHeight(activity) + "x" + getScreenWidth(activity) + " px)";
    }


    public void fillSessionWithDummyData(Context context) {
        try {

            for (int x = 0; x < 10; x++) {
                RoundSettings rs = new RoundSettings("CHI 2003", 3, true, true, false, true, true, false);
                Round r = mSession.getRound(prepareNewRound(rs, context));
                r.getRoundSettings().setKeyboard(getKeyboardName(context));
                for (Phrase p : r.getPhrases()) {
                    for (int i = 0; i < 10; i++) {
                        p.addKeystroke(1 + i, 'a', false);
                    }
                    p.setTranscribed(p.getInputStream());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}


//                } else {
//
//                    if ((before > 0 && before > count) || (count == 0)) {
////                    Log.d(LOGTAG, "<BACKSPACE>" + "s=" + s + ", start=" + start + ", before=" + before + ", " +
////                            "count=" + count);
//                        // DEBUG
//                        log(s.toString(), start, before, count, actualCount, ' ', "<BACKSPACE>");
//                        // DEBUG
//                        mRound.getCurrentPhrase().addKeystroke(mStopWatch.getCurrentMs(), Character.toChars(8)[0]);
//                    } else {
//                        int lastCharPos = s.length() - 1;
//                        if (lastCharPos >= 0) {
//                            char lastChar = s.charAt(lastCharPos);
//                            switch (lastChar) {
//                                case '\n':
//                                    String transcribed = s.toString();
//                                    // Substring to remove the \n at the end
//                                    mRound.getCurrentPhrase().setTranscribed(
//                                            transcribed.substring(0, transcribed.length() - 1));
//                                    fillPresentedWithNextPhrase(false);
////                                Log.d(LOGTAG, "<RETURN>" + " (" + mStopWatch.getCurrentMs() + ")" + "s=" +
////                                        transcribed.substring(0, transcribed.length() - 1) + "<RETURN>, " +
////                                        "start=" + start + ", before=" + before + ", count=" + count);
//                                    // DEBUG
//                                    log(s.toString(), start, before, count, actualCount, ' ', "<RETURN>");
//                                    // DEBUG
//                                    break;
//                                default:
//                                    // DEBUG
//                                    log(s.toString(), start, before, count, actualCount, lastChar,
//                                            ((count - before) > 1 || (count - before) == 0) ? "***** AUTO-SUGGEST *****" : "");
//                                    // DEBUG
//                                    mRound.getCurrentPhrase().addKeystroke(mStopWatch.getCurrentMs(), lastChar);
//                                    break;
//                            }
//                        }
//                    }
//                }