package com.szoldapps.tealib.time;

import android.os.Handler;
import android.widget.TextView;

/**
 * This class handles the time component of the TeaLib.
 * Created by thomasszoldatits on 15.12.14.
 */
public class StopWatch {

    private final TextView mTimer;
    private Handler mHandler = new Handler();
    private String mCurrentTime = "";
    private long startTime;
    private long stopTime;
    private long elapsedTime = 0;
    private final int REFRESH_RATE = 10;
    private String hours, minutes, seconds, milliseconds;
    private long secs, mins, hrs;
    private boolean paused = true;
    private long mCurrentMs = 0;
    private long pauseTime;
    private boolean first=true;
    private long roundStartTime;

    public StopWatch(TextView timer) {
        this.mTimer = timer;
    }

    private Runnable startTimer = new Runnable() {
        public void run() {
            elapsedTime = System.currentTimeMillis() - startTime;
            updateTimer(elapsedTime);
            mHandler.postDelayed(this, REFRESH_RATE);
        }
    };

    public void start() {
        if (elapsedTime > 0) {
            pauseTime += elapsedTime;
        }
        // TODO figure out correct org. start time
        startTime = System.currentTimeMillis() - elapsedTime;
        if(first) {
            roundStartTime = startTime;
            first=false;
        }
        paused = false;
        mHandler.removeCallbacks(startTimer);
        mHandler.postDelayed(startTimer, 0);
    }

    public void pause() {
        mHandler.removeCallbacks(startTimer);
        paused = true;
    }

    public void stop() {
        mHandler.removeCallbacks(startTimer);
        stopTime = System.currentTimeMillis();
        pauseTime = 0;
        paused = false;
        mCurrentTime = "00:00:00.00";
        mTimer.setText(mCurrentTime);
    }

    public String getCurrentTime() {
        return mCurrentTime;
    }


    public long getCurrentMs() {
        return mCurrentMs;
    }

    private void updateTimer(float time) {
        secs = (long) (time / 1000);
        mins = (long) ((time / 1000) / 60);
        hrs = (long) (((time / 1000) / 60) / 60);

		/* Convert the seconds to String
         * and format to ensure it has
		 * a leading zero when required
		 */
        secs = secs % 60;
        seconds = String.valueOf(secs);
        if (secs == 0) {
            seconds = "00";
        } else if (secs > 0 && secs < 10) {
            seconds = "0" + seconds;
        }

		/* Convert the minutes to String and format the String */
        mins = mins % 60;
        minutes = String.valueOf(mins);
        if (mins == 0) {
            minutes = "00";
        } else if (mins > 0 && mins < 10) {
            minutes = "0" + minutes;
        }

    	/* Convert the hours to String and format the String */
        hours = String.valueOf(hrs);
        if (hrs == 0) {
            hours = "00";
        } else if (hrs > 0 && hrs < 10) {
            hours = "0" + hours;
        }

        /* Milliseconds */
        milliseconds = String.valueOf((long) time);
        if (milliseconds.length() == 2) {
            milliseconds = "0" + milliseconds;
        } else if (milliseconds.length() <= 1) {
            milliseconds = "00" + milliseconds;
        }
        milliseconds = milliseconds.substring(milliseconds.length() - 3, milliseconds.length() - 1);

        mCurrentMs = (long) time;
        /* Setting the timer text to the elapsed time */
        mCurrentTime = hours + ":" + minutes + ":" + seconds + "." + milliseconds;
        mTimer.setText(mCurrentTime);
    }

    public long getStartTime() {
        first = true;
        return roundStartTime;
    }

    public long getStopTime() {
        return stopTime;
    }

    public boolean isPaused() {
        return paused;
    }

    public long getPauseTimeAndReset() {
        long ret = pauseTime;
        pauseTime = 0;
        return ret;
    }
}
