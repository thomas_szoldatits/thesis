package com.szoldapps.thesis.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.szoldapps.tealib.other.Settings;
import com.szoldapps.tealib.stats.Metrics;
import com.szoldapps.tealib.stats.PhraseStats;
import com.szoldapps.tealib.stats.RoundStats;
import com.szoldapps.tealib.stats.SessionStats;
import com.szoldapps.thesis.R;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Adapter to display the stats in {@link com.szoldapps.thesis.activity.StatsActivity}.
 *
 * Created by thomasszoldatits on 25.02.15.
 */
@SuppressLint("InflateParams")
public class StatsListViewAdapter extends BaseAdapter {

    private final SessionStats mSessionStats;
    private final List<RoundStats> mRoundStats;
    private final Context mContext;
    private final LayoutInflater mInflater;

    public StatsListViewAdapter(SessionStats sessionStats, Context context) {
        this.mSessionStats = sessionStats;
        this.mRoundStats = mSessionStats.getRoundStats();
        this.mContext = context;
        this.mInflater = LayoutInflater.from(mContext);
    }

    public View getHeader() {
        // Inflate Header
        View header = mInflater.inflate(R.layout.lv_header, null);

        // This Part is ALWAYS SHOWN
        TextView tvSessionHL = (TextView) header.findViewById(R.id.tv_session_name);
        tvSessionHL.setText(String.format(mContext.getString(R.string.session), mSessionStats.getName(),
                mSessionStats.getDevice(),
                mSessionStats.getScreenSize()));

        // Add Session Summary
        fillSessionStatsHSV(header);
        return header;
    }

    private void fillSessionStatsHSV(View header) {
        TableLayout tl = (TableLayout) header.findViewById(R.id.tl_session);

        // Set Session HL
        TextView tvRoundNr = (TextView) header.findViewById(R.id.tv_round_nr);
        tvRoundNr.setText(mContext.getString(R.string.session_summary));

        // Add Header Row
        tl.addView(getHeaderRow(true));

        // Fill SessionTable with RoundStats
        TextView tv;
        for (RoundStats rs : mSessionStats.getRoundStats()) {
            TableRow row = new TableRow(mContext);
            // Round Nr
            tv = (TextView) LayoutInflater.from(mContext).inflate(R.layout.item_table_gravity_left, null);
            tv.setText("Round " + rs.getNr());
            row.addView(tv);

            tv = (TextView) LayoutInflater.from(mContext).inflate(R.layout.item_table_gravity_left, null);
            tv.setText(rs.getKeyboard());
            row.addView(tv);

            // Add Metrics to Row
            row = addMetricsToRow(row, rs.getMetrics());

            // add Row to Table
            tl.addView(row);
        }
    }

    @Override
    public int getCount() {
        return mRoundStats.size();
    }

    @Override
    public Object getItem(int position) {
        return mRoundStats.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder mViewHolder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.lv_round, null);
            mViewHolder = new MyViewHolder();
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        RoundStats rs = (RoundStats) getItem(position);
        mViewHolder.tvRoundNr = detail(convertView, R.id.tv_round_nr,
                String.format(mContext.getString(R.string.round_keyboard), rs.getNr(),rs.getKeyboard()));
        mViewHolder.tlRound = getRoundStatsTable(convertView, R.id.tl_session, rs);

        return convertView;
    }

    private class MyViewHolder {
        TextView tvRoundNr;
        TableLayout tlRound;
    }

    private TextView detail(View v, int resId, String text) {
        TextView tv = (TextView) v.findViewById(resId);
        tv.setText(text);
        return tv;
    }

    private TableLayout getRoundStatsTable(View v, int resId, RoundStats rs) {
        TableLayout tl = (TableLayout) v.findViewById(resId);
        tl.removeAllViews();
        // Add Header Row
        tl.addView(getHeaderRow(false));

        TextView tv;
        for (PhraseStats ps : rs.getPhraseStats()) {
            TableRow row = new TableRow(mContext);
            // Presented (P)
            tv = (TextView) LayoutInflater.from(mContext).inflate(R.layout.item_table_gravity_left, null);
            tv.setText(ps.getPresented());
            row.addView(tv);
            // Transcribed (T)
            tv = (TextView) LayoutInflater.from(mContext).inflate(R.layout.item_table_gravity_left, null);
            tv.setText(ps.getTranscribed());
            row.addView(tv);

            // Add Metrics to Row
            row = addMetricsToRow(row, ps.getMetrics());

            // add Row to Table
            tl.addView(row);
        }

        return tl;
    }

    private TableRow getHeaderRow(boolean sessionHeader) {
        TableRow row = new TableRow(mContext);
        TextView tv;
        if (sessionHeader) {
            tv = (TextView) mInflater.inflate(R.layout.item_table_header, null);
            tv.setText(mContext.getString(R.string.round));
            tv.setGravity(Gravity.START);
            row.addView(tv);
            tv = (TextView) mInflater.inflate(R.layout.item_table_header, null);
            tv.setText(mContext.getString(R.string.keyboard));
            tv.setGravity(Gravity.START);
            row.addView(tv);
        } else {
            tv = (TextView) mInflater.inflate(R.layout.item_table_header, null);
            tv.setText(mContext.getString(R.string.presented_p));
            tv.setGravity(Gravity.START);
            row.addView(tv);
            tv = (TextView) mInflater.inflate(R.layout.item_table_header, null);
            tv.setText(mContext.getString(R.string.transcribed_t));
            tv.setGravity(Gravity.START);
            row.addView(tv);
        }
        String[] metricHLs = {mContext.getString(R.string.p_chars), mContext.getString(R.string.t_chars),
                mContext.getString(R.string.input_time_in_s), mContext.getString(R.string.pause_time_in_s),
                mContext.getString(R.string.total_time_in_s), mContext.getString(R.string.wpm),
                mContext.getString(R.string.msd), mContext.getString(R.string.msd_er),  mContext.getString(R.string
                .bs_count),
                mContext.getString(R.string.kspc), mContext.getString(R.string.c),
                mContext.getString(R.string.inf), mContext.getString(R.string.f),
                mContext.getString(R.string.If), mContext.getString(R.string.cer_percent),
                mContext.getString(R.string.uer_percent), mContext.getString(R.string.ter_percent),
                mContext.getString(R.string.correff), mContext.getString(R.string.partcon),
                mContext.getString(R.string.utilbandwith), mContext.getString(R.string.wastedbandwith)};
        for (String s : metricHLs) {
            tv = (TextView) mInflater.inflate(R.layout.item_table_header, null);
            tv.setText(s);
            row.addView(tv);
        }
        return row;
    }


    private TableRow addMetricsToRow(TableRow row, Metrics m) {
        TextView tv;
        // P chars
        tv = (TextView) mInflater.inflate(R.layout.item_table_gravity_right, null);
        tv.setText(Integer.toString(m.getpChars()));
        row.addView(tv);
        // T chars
        tv = (TextView) mInflater.inflate(R.layout.item_table_gravity_right, null);
        tv.setText(Integer.toString(m.gettChars()));
        row.addView(tv);
        // Input Time (in s)
        tv = (TextView) mInflater.inflate(R.layout.item_table_gravity_right, null);
        tv.setText(toDec(m.getInputTime()));
        row.addView(tv);
        // Pause Time (in s)
        tv = (TextView) mInflater.inflate(R.layout.item_table_gravity_right, null);
        tv.setText(toDec(m.getPauseTime()));
        row.addView(tv);
        // Total Time (in s)
        tv = (TextView) mInflater.inflate(R.layout.item_table_gravity_right, null);
        tv.setText(toDec(m.getTotalTime()));
        row.addView(tv);
        // WPM
        tv = (TextView) mInflater.inflate(R.layout.item_table_gravity_right, null);
        tv.setText(toDec(m.getWpm()));
        row.addView(tv);
        // MSD
        tv = (TextView) mInflater.inflate(R.layout.item_table_gravity_right, null);
        tv.setText(Integer.toString(m.getMsd()));
        row.addView(tv);
        // MSD ER
        tv = (TextView) mInflater.inflate(R.layout.item_table_gravity_right, null);
        tv.setText(toDec(m.getMsdEr() * 100));
        row.addView(tv);
        // BsCount
        tv = (TextView) mInflater.inflate(R.layout.item_table_gravity_right, null);
        tv.setText(Integer.toString(m.getBsCount()));
        row.addView(tv);
        // KSPC
        tv = (TextView) mInflater.inflate(R.layout.item_table_gravity_right, null);
        tv.setText(toDec(m.getKspc()));
        row.addView(tv);
        // C
        tv = (TextView) mInflater.inflate(R.layout.item_table_gravity_right, null);
        tv.setText(Integer.toString(m.getC()));
        row.addView(tv);
        // INF
        tv = (TextView) mInflater.inflate(R.layout.item_table_gravity_right, null);
        tv.setText(Integer.toString(m.getInf()));
        row.addView(tv);
        // F
        tv = (TextView) mInflater.inflate(R.layout.item_table_gravity_right, null);
        tv.setText(Integer.toString(m.getF()));
        row.addView(tv);
        // IF
        tv = (TextView) mInflater.inflate(R.layout.item_table_gravity_right, null);
        tv.setText(Integer.toString(m.getIf()));
        row.addView(tv);
        // CER (%)
        tv = (TextView) mInflater.inflate(R.layout.item_table_gravity_right, null);
        tv.setText(toDec(m.getCer() * 100));
        row.addView(tv);
        // UER (%)
        tv = (TextView) mInflater.inflate(R.layout.item_table_gravity_right, null);
        tv.setText(toDec(m.getUer() * 100));
        row.addView(tv);
        // TER (%)
        tv = (TextView) mInflater.inflate(R.layout.item_table_gravity_right, null);
        tv.setText(toDec(m.getTer() * 100));
        row.addView(tv);
        // CorrEff (%)
        tv = (TextView) mInflater.inflate(R.layout.item_table_gravity_right, null);
        tv.setText(toDec(m.getCorrEff() * 100));
        row.addView(tv);
        // PartCon (%)
        tv = (TextView) mInflater.inflate(R.layout.item_table_gravity_right, null);
        tv.setText(toDec(m.getPartCon() * 100));
        row.addView(tv);
        // UtilBandwidth (%)
        tv = (TextView) mInflater.inflate(R.layout.item_table_gravity_right, null);
        tv.setText(toDec(m.getUtilBandwidth() * 100));
        row.addView(tv);
        // WastedBandwidth (%)
        tv = (TextView) mInflater.inflate(R.layout.item_table_gravity_right, null);
        tv.setText(toDec(m.getWastedBandwidth() * 100));
        row.addView(tv);

        return row;
    }

    private DecimalFormat df = new DecimalFormat(Settings.DECIMAL_FORMAT_FOR_STATS_ACTIVITY);

    private String toDec(double d) {
        return df.format(d);
    }
}
