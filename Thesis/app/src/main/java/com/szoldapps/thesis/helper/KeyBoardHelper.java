package com.szoldapps.thesis.helper;

import android.app.Activity;
import android.util.Log;
import android.view.WindowManager;

/**
 * Opens or closes soft keyboard
 */
public class KeyBoardHelper {

	private static final String LOGTAG = KeyBoardHelper.class.getSimpleName();

    /**
     * Opens soft keyboard.
     * @param activity  activity
     */
	public static void showKeyboard(Activity activity) {
		Log.v(LOGTAG, "showKeyboard");
		activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
	}

    @SuppressWarnings("unused")
	public static void hideKeyboard(Activity activity) {
		Log.v(LOGTAG, "hideKeyboard");
		activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
	}
}
