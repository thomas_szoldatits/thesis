package com.szoldapps.tealib.stats;

import com.szoldapps.tealib.model.RoundSettings;

import java.util.List;

/**
 * Created by thomasszoldatits on 24.02.15.
 */
public class RoundStats {

    private final String phraseSet;
    private final int phraseCount;
    private final boolean lowerCaseOnly;
    private final boolean alphaNumOnly;
    private final boolean showSuggestions;
    private final boolean showNextBtn;
    private int nr;
    private long startTime;
    private long endTime;
    private String keyboard;
    private List<PhraseStats> phraseStats;
    private Metrics metrics;

    public RoundStats(int roundNr, long startTime, long endTime, RoundSettings roundSettings) {
        this.nr = roundNr;
        this.startTime = startTime;
        this.endTime = endTime;
        this.phraseSet = roundSettings.getPhraseSetName();
        this.phraseCount = roundSettings.getPhraseCount();
        this.lowerCaseOnly = roundSettings.isLowerCaseOnly();
        this.alphaNumOnly = roundSettings.isAlphaNumOnly();
        this.keyboard = roundSettings.getKeyboard();
        this.showSuggestions = roundSettings.isShowSuggestions();
        this.showNextBtn = roundSettings.isShowNextBtn();
    }

    public int getNr() {
        return nr;
    }

    public void setNr(int nr) {
        this.nr = nr;
    }

    public List<PhraseStats> getPhraseStats() {
        return phraseStats;
    }

    public void setPhraseStats(List<PhraseStats> phraseStats) {
        this.phraseStats = phraseStats;
    }

    public Metrics getMetrics() {
        return metrics;
    }

    public void setMetrics(Metrics metrics) {
        this.metrics = metrics;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getPhraseSet() {
        return phraseSet;
    }

    public int getPhraseCount() {
        return phraseCount;
    }

    public boolean isLowerCaseOnly() {
        return lowerCaseOnly;
    }

    public boolean isAlphaNumOnly() {
        return alphaNumOnly;
    }

    public boolean isShowSuggestions() {
        return showSuggestions;
    }

    public boolean isShowNextBtn() {
        return showNextBtn;
    }

    public String getKeyboard() {
        return keyboard;
    }

    public void setKeyboard(String keyboard) {
        this.keyboard = keyboard;
    }

}
