package com.szoldapps.tealib.stats;

import android.widget.TextView;

import java.util.List;

/**
 * Created by thomasszoldatits on 24.02.15.
 */
public class SessionStats {

    private String name;
    private String device;
    private List<RoundStats> roundStats;
    private Metrics metrics;
    private long startTime;
    private long endTime;
    private String screenSize;

    public SessionStats(String name, String device, String screenSize) {
        this.name = name;
        this.device = device;
        this.screenSize = screenSize;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public List<RoundStats> getRoundStats() {
        return roundStats;
    }

    public void setRoundStats(List<RoundStats> roundStats) {
        this.roundStats = roundStats;
    }

    public Metrics getMetrics() {
        return metrics;
    }

    public void setMetrics(Metrics metrics) {
        this.metrics = metrics;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getScreenSize() {
        return screenSize;
    }
}
