package com.szoldapps.tealib.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * TeaEditText provides an EditText view that does not allow the user to make a selection. In other words the user
 * cannot change the position of the cursor. it will always stay at the last position.
 *
 * Created by thomasszoldatits on 16.02.15.
 */
public class TeaEditText extends EditText {

    @SuppressWarnings("unused")
    private static final String LOGTAG = TeaEditText.class.getSimpleName();

    public TeaEditText(Context context) {
        super(context);
    }

    public TeaEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TeaEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void onSelectionChanged(int start, int end) {

        CharSequence text = getText();
        if (text != null) {
            if (start != text.length() || end != text.length()) {
                setSelection(text.length(), text.length());
                return;
            }
        }

        super.onSelectionChanged(start, end);
    }

}
