package com.szoldapps.thesis.helper;

import android.app.Activity;
import android.content.Intent;

import com.szoldapps.thesis.R;
import com.szoldapps.thesis.activity.MainActivity;
import com.szoldapps.thesis.activity.RoundActivity;
import com.szoldapps.thesis.activity.SessionActivity;
import com.szoldapps.thesis.activity.StatsActivity;

public class LinkingHelper {

	private final static int ANIM_ENTER_RIGHT_EXIT_LEFT = 0;
	private final static int ANIM_ENTER_LEFT_EXIT_RIGHT = 1;
	private final static int ANIM_ENTER_BOTTOM_EXIT_TOP = 2;
	
	private static void animation(Activity activity, int animation) {
		switch (animation) {
		case ANIM_ENTER_RIGHT_EXIT_LEFT:
			activity.overridePendingTransition(R.anim.enter_right_slide, R.anim.exit_left_slide);
			break;
		case ANIM_ENTER_LEFT_EXIT_RIGHT:
			activity.overridePendingTransition(R.anim.enter_left_slide, R.anim.exit_right_slide);
			break;
		case ANIM_ENTER_BOTTOM_EXIT_TOP:
			activity.overridePendingTransition(R.anim.slide_bottom_up, R.anim.slide_top_up);
			break;
		default:
			break;
		}
	}

	public static void goBack(Activity activity) {
		activity.onBackPressed();
		animation(activity, ANIM_ENTER_LEFT_EXIT_RIGHT);
	}


    public static void goToSessionActivity(Activity activity) {
        Intent intent = new Intent(activity, SessionActivity.class);
        intent.putExtra(SessionActivity.EXTRA_COMING_FROM_ANOTHER_ACTIVITY, true);
        activity.startActivity(intent);
        animation(activity, ANIM_ENTER_LEFT_EXIT_RIGHT);
        activity.finish();
    }

	/**
	 * Opens {@link com.szoldapps.thesis.activity.RoundActivity} with {@link #ANIM_ENTER_LEFT_EXIT_RIGHT}.
	 * 
	 * @param activity
	 */
	public static void goToRoundActivity(Activity activity) {
        Intent intent = new Intent(activity, RoundActivity.class);
		activity.startActivity(intent);
		animation(activity, ANIM_ENTER_RIGHT_EXIT_LEFT);
        activity.finish();
	}

    public static void goToMainActivity(Activity activity, int roundNr, boolean showNextBtn) {
        Intent intent = new Intent(activity, MainActivity.class);
        intent.putExtra(MainActivity.EXTRA_ROUND_NR, roundNr);
        intent.putExtra(MainActivity.EXTRA_SHOW_NEXT_BTN, showNextBtn);
        activity.startActivity(intent);
        animation(activity, ANIM_ENTER_RIGHT_EXIT_LEFT);
        activity.finish();
    }

    public static void goToStatsActivity(Activity activity, boolean showNextBtn) {
        Intent intent = new Intent(activity, StatsActivity.class);
        intent.putExtra(MainActivity.EXTRA_SHOW_NEXT_BTN, showNextBtn);
        activity.startActivity(intent);
        animation(activity, ANIM_ENTER_RIGHT_EXIT_LEFT);
        activity.finish();
    }

    public static void closeApp(Activity activity) {
        activity.finish();
    }
}
