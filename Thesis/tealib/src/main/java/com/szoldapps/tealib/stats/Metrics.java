package com.szoldapps.tealib.stats;

/**
 * Created by thomasszoldatits on 24.02.15.
 */
public class Metrics {

    private int pChars, tChars;
    private double inputTime, pauseTime, totalTime;
    private double wpm, kspc;
    private int msd, bsCount;
    private int c, inf, f, iF;
    private double cer, uer, ter;
    private double corrEff, partCon, utilBandwidth, wastedBandwidth;
    private double msdEr;

    // for PhraseStats
    public Metrics() {

    }

    // for RoundStats
    public Metrics(int pChars, int tChars, double inputTime, double pauseTime, double totalTime, double wpm, int msd,
                   double msdEr, int bsCount, double kspc, int c, int inf, int f, int iF, double cer, double uer,
                   double ter, double corrEff, double partCon, double utilBandwidth, double wastedBandwidth) {
        this.pChars = pChars;
        this.tChars = tChars;
        this.inputTime = inputTime;
        this.pauseTime = pauseTime;
        this.totalTime = totalTime;
        this.wpm = wpm;
        this.msd = msd;
        this.msdEr = msdEr;
        this.bsCount = bsCount;
        this.kspc = kspc;
        this.c = c;
        this.inf = inf;
        this.f = f;
        this.iF = iF;
        this.cer = cer;
        this.uer = uer;
        this.ter = ter;
        this.corrEff = corrEff;
        this.partCon = partCon;
        this.utilBandwidth = utilBandwidth;
        this.wastedBandwidth = wastedBandwidth;
    }

    public int getpChars() {
        return pChars;
    }

    public void setpChars(int pChars) {
        this.pChars = pChars;
    }

    public int gettChars() {
        return tChars;
    }

    public void settChars(int tChars) {
        this.tChars = tChars;
    }

    public double getInputTime() {
        return inputTime;
    }

    public void setInputTime(double inputTime) {
        this.inputTime = inputTime;
    }

    public double getPauseTime() {
        return pauseTime;
    }

    public void setPauseTime(double pauseTime) {
        this.pauseTime = pauseTime;
    }

    public double getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(double totalTime) {
        this.totalTime = totalTime;
    }

    public double getWpm() {
        return wpm;
    }

    public void setWpm(double wpm) {
        this.wpm = wpm;
    }

    public int getMsd() {
        return msd;
    }

    public void setMsd(int msd) {
        this.msd = msd;
    }

    public double getMsdEr() {
        return msdEr;
    }

    public void setMsdEr(double msdEr) {
        this.msdEr = msdEr;
    }

    public int getBsCount() {
        return bsCount;
    }

    public void setBsCount(int bsCount) {
        this.bsCount = bsCount;
    }

    public double getKspc() {
        return kspc;
    }

    public void setKspc(double kspc) {
        this.kspc = kspc;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public int getInf() {
        return inf;
    }

    public void setInf(int inf) {
        this.inf = inf;
    }

    public int getF() {
        return f;
    }

    public void setF(int f) {
        this.f = f;
    }

    public int getIf() {
        return iF;
    }

    public void setIf(int iF) {
        this.iF = iF;
    }

    public double getCer() {
        return cer;
    }

    public void setCer(double cer) {
        this.cer = cer;
    }

    public double getUer() {
        return uer;
    }

    public void setUer(double uer) {
        this.uer = uer;
    }

    public double getTer() {
        return ter;
    }

    public void setTer(double ter) {
        this.ter = ter;
    }

    public double getCorrEff() {
        return corrEff;
    }

    public void setCorrEff(double corrEff) {
        this.corrEff = corrEff;
    }

    public double getPartCon() {
        return partCon;
    }

    public void setPartCon(double partCon) {
        this.partCon = partCon;
    }

    public double getUtilBandwidth() {
        return utilBandwidth;
    }

    public void setUtilBandwidth(double utilBandwidth) {
        this.utilBandwidth = utilBandwidth;
    }

    public double getWastedBandwidth() {
        return wastedBandwidth;
    }

    public void setWastedBandwidth(double wastedBandwidth) {
        this.wastedBandwidth = wastedBandwidth;
    }

}
