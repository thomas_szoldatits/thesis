package com.szoldapps.tealib.model;

import com.szoldapps.tealib.TeaMetrics;
import com.szoldapps.tealib.other.Settings;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thomasszoldatits on 16.02.15.
 */
public class Phrase {

    private String presented;
    private String transcribed;
    private List<Keystroke> keystrokes;
    private long startTime;
    private long pauseTime;
    private long endTime = -1;

    // Stats
    private Integer msd;
    private Integer c;
    private Integer inf;
    private Integer f;
    private Integer iF;
    private Double cer;
    private Double uer;
    private Double ter;

    private Double mCorrEff;
    private Double mPartCon;
    private Double mUtilBandwidth;
    private Double mWastedBandwidth;
    private Double mMsdEr;

    public Phrase(String presented) {
        this.presented = presented;
        this.keystrokes = new ArrayList<Keystroke>();
    }

    public String getPresented() {
        return presented;
    }

    public void setPresented(String presented) {
        this.presented = presented;
    }

//    public void addBackspaceKeystroke(long time) {

    public void addKeystroke(long time, char c, boolean autoAdded) {
        if (Settings.isBackspace(c)) {
            for (int i = keystrokes.size() - 1; i >= 0; i--) {
                Keystroke k = keystrokes.get(i);
                if (k.isInT()) {
                    k.setInT(false);
                    break;
                }
            }
        }
        //Log.i("phrase", "added: " + c);
        keystrokes.add(new Keystroke(time, c, autoAdded));
    }

    /**
     * This is just here because the SwiftKey Keyboard adds an unnecessary Space char if you accept a suggestion
     * at the end of a phrase. Therefore this method deletes the last space char.
     */
    public void deleteLastKeystrokeIfSpaceChar() {
        int lastLocation = keystrokes.size() - 1;
        if (lastLocation > 0) {
            Keystroke last = keystrokes.get(lastLocation);
            if (last.getChar() == ' ') {
                // remove from IS
                keystrokes.remove(lastLocation);
                // remove from T
                transcribed = transcribed.substring(0, transcribed.length()-1);
            }
        }
    }

    public String getInputStream() {
        String ret = "";
        for (Keystroke k : keystrokes) {
            ret += k.getChar();
        }
        return ret;
    }

    public String getTranscribed() {
        return transcribed;
    }

    public void setTranscribed(String transcribed) {
        this.transcribed = transcribed;
    }

    public List<Keystroke> getKeystrokes() {
        return keystrokes;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public void setPauseTime(long pauseTime) {
        this.pauseTime = pauseTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public long getInputTimeInMs() {
        return endTime - startTime;
    }

    public long getPauseTimeInMs() {
        return pauseTime;
    }

    public long getTotalTimeInMs() {
        return getInputTimeInMs() + getPauseTimeInMs();
    }

    public double getInputTimeInS() {
        return getInputTimeInMs() / (double) 1000;
    }

    public double getPauseTimeInS() {
        return getPauseTimeInMs() / (double) 1000;
    }

    public double getTotalTimeInS() {
        return getTotalTimeInMs() / (double) 1000;
    }

    public int getMSD() {
        if (msd != null) {
            return msd;
        }
        if (presented != null && transcribed != null) {
            return msd = TeaMetrics.calcLevenshteinDistance(presented, transcribed);
        }
        return -1;
    }

    public double getWordsPerMinute() {
        return TeaMetrics.calcWordsPerMinute(transcribed.length(), getInputTimeInS());
    }

    public int getBackspaceCount() {
        return TeaMetrics.getBackspaceCount(this.getKeystrokes());
    }

    public double getKeystrokesPerCharacter() {
        return TeaMetrics.calcKeystrokesPerCharacter(getInputStream().length(), getTranscribed().length());
    }

    public int getCorrect() {
        if (c == null) {
            return c = Math.max(presented.length(), transcribed.length()) - getMSD();
        }
        return c;
    }

    public int getIncorrectNotFixed() {
        if (inf == null) {
            return inf = getMSD();
        }
        return inf;
    }

    public int getFixed() {
        if (f == null) {
            return f = this.getBackspaceCount();
        }
        return f;
    }

    public int getIncorrectFixed() {
        if (iF == null) {
            return iF = TeaMetrics.getIncorrectFixed(keystrokes);
        }
        return iF;
    }

    public double getCorrectedErrorRate() {
        if (cer == null) {
            return cer = TeaMetrics.getCorrectedErrorRate(getCorrect(), getIncorrectNotFixed(), getIncorrectFixed());
        }
        return cer;
    }

    public double getUncorrectedErrorRate() {
        if (uer == null) {
            return uer =
                    TeaMetrics.getUncorrectedErrorRate(getCorrect(), getIncorrectNotFixed(), getIncorrectFixed());
        }
        return uer;
    }

    /**
     * Returns TER = ((INF+F)/(C+INF+IF)) = CER+UER
     *
     * @return TER
     */
    public double getTotalErrorRate() {
        if (ter == null) {
            return ter = getCorrectedErrorRate() + getUncorrectedErrorRate();
        }
        return ter;
    }

    /**
     * Returns Correction Efficiency = IF/F
     *
     * @return CorrEff
     */
    public Double getCorrEff() {
        if (mCorrEff == null) {
            double corrEff = (double) getIncorrectFixed() / (double) getFixed();
            return mCorrEff = Double.isNaN(corrEff) ? 1 : corrEff;  // 1 because 0/0 is NaN and equals 100% CorrEff
        }
        return mCorrEff;
    }

    /**
     * Returns Participant Conscientiousness = IF/(IF+INF)
     *
     * @return PartCon
     */
    public Double getPartCon() {
        if (mPartCon == null) {
            double partCon = (double) getIncorrectFixed() / ((double) getIncorrectFixed() + (double) getIncorrectNotFixed());
            return mPartCon = Double.isNaN(partCon) ? 1 : partCon;  // 1 because 0/0 is NaN and equals 100% PartCon
        }
        return mPartCon;
    }

    /**
     * Returns Utilized Bandwidth = C / (C + INF + IF + F)
     *
     * @return UtilBandwidth
     */
    public Double getUtilBandwidth() {
        if (mUtilBandwidth == null) {
            return mUtilBandwidth = (double) getCorrect() / ((double) getCorrect() + (double) getIncorrectNotFixed() +
                    (double) getIncorrectFixed() + (double) getFixed());
        }
        return mUtilBandwidth;
    }

    /**
     * Returns Wasted Bandwidth = (INF + IF + F) / (C + INF + IF + F)
     *
     * @return WastedBandwidth
     */
    public Double getWastedBandwidth() {
        if (mWastedBandwidth == null) {
            return mWastedBandwidth = ((double) getIncorrectNotFixed() + (double) getIncorrectFixed() + (double)
                    getFixed()) / (
                    (double) getCorrect() + (double) getIncorrectNotFixed() + (double) getIncorrectFixed() +
                            (double) getFixed());
        }
        return mWastedBandwidth;
    }

    /**
     * Returns MSD ER = INF / (C + INF)
     *
     * @return MSD ER
     */
    public Double getMsdEr() {
        if (mMsdEr == null) {
            return mMsdEr = (((double) getIncorrectNotFixed()) / ((double) getCorrect() + (double)
                    getIncorrectNotFixed()));
        }
        return mMsdEr;
    }

}
