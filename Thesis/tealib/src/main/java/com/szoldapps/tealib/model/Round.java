package com.szoldapps.tealib.model;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by thomasszoldatits on 16.02.15.
 */
public class Round {

    private static final String LOGTAG = Round.class.getSimpleName();

    private final int roundNr;
    private final RoundSettings roundSettings;
    private long startTime;
    private long endTime;

    private List<Phrase> phrases;
    private int phraseCounter = -1;

    public Round(int roundNr, RoundSettings roundSettings, Context context) throws IOException {
        this.roundNr = roundNr;
        this.roundSettings = roundSettings;

        loadPhrases(roundSettings.getPhraseSetName(), roundSettings.getPhraseCount(), context);
    }

    private void loadPhrases(String phraseSetName, int phraseCount, Context context) throws IOException {
        List<String> phrases = loadPhraseSetFromAssetsDir(phraseSetName, context);
        this.phrases = getRandomPhrases(phraseCount, phrases);
    }

    private List<String> loadPhraseSetFromAssetsDir(String phraseSetName, Context context) throws IOException {
        BufferedReader br = new BufferedReader(
                new InputStreamReader(context.getAssets().open("phrasesets/"+phraseSetName+".txt")));
        List<String> phraseSet = new ArrayList<String>();
        String line;
        while ((line = br.readLine()) != null) {
            phraseSet.add(line);
//            Log.d(LOGTAG, line);
        }
        br.close();

        return phraseSet;
    }

    private List<Phrase> getRandomPhrases(int phraseCount, List<String> allPhrases) {
        List<Phrase> list = new ArrayList<Phrase>();
        int allPhrasesSize = allPhrases.size();
        for (int i = 0; i < phraseCount; i++) {
            list.add(new Phrase(allPhrases.get(new Random().nextInt(allPhrasesSize))));
        }
        return list;
    }

    public Phrase getCurrentPhrase() {
        if(phraseCounter >= 0 && phraseCounter < roundSettings.getPhraseCount()) {
            return phrases.get(phraseCounter);
        } else {
            return null;
        }
    }

    public Phrase getNextPhrase() {
        if(phraseCounter >= roundSettings.getPhraseCount()-1) {
            // TODO: Throw NoMorePhrasesLeftException;
            return null;
        }
        return phrases.get(++phraseCounter);
    }

    public int getRoundNr() {
        return roundNr;
    }

    public List<Phrase> getPhrases() {
        return phrases;
    }


    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long mStartTime) {
        this.startTime = mStartTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long mEndTime) {
        this.endTime = mEndTime;
    }

    public RoundSettings getRoundSettings() {
        return roundSettings;
    }
}
