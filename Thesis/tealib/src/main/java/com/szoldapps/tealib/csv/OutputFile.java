package com.szoldapps.tealib.csv;

import java.util.List;

public interface OutputFile {

    /**
     * Returns a list of strings.
     * @return  List<String> that represent the content.
     */
	public List<String> getContentRows();

    /**
     * Returns the filename (without the path)
     * @return  filename only!
     */
	public String getFileName();
}
