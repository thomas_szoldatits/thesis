package com.szoldapps.thesis.activity;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.szoldapps.tealib.TeaLib;
import com.szoldapps.tealib.model.RoundSettings;
import com.szoldapps.thesis.helper.KeyBoardHelper;
import com.szoldapps.thesis.helper.LinkingHelper;
import com.szoldapps.thesis.R;

import java.io.IOException;
import java.util.List;


public class RoundActivity extends ActionBarActivity {

    private static final String LOGTAG = RoundActivity.class.getSimpleName();
    private ScrollView mSvMain;
    private TextView mTvSessionName;
    private Spinner mSpPhraseSet;
    private EditText mEtPhraseCount;
    private Switch mSwLowercase;
    private Switch mSwAlphaNum;
    private Switch mSwAutoCapFirst;
    private TextView mTvCurKeyboard;
    private Button mBtnKeyboard;
    private Switch mSwSuggestions;
    private Switch mSwNextBtn;
    private Switch mSwTraining;

    private Button mBtnStart;
    private String mDefaultPhraseCount = "5";
    private String mTrainingPhraseCount = "2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_round);

        mSvMain = (ScrollView) findViewById(R.id.round_sv_main);
        mTvSessionName = (TextView) findViewById(R.id.round_txt_caption);
        mSpPhraseSet = (Spinner) findViewById(R.id.round_sp_phrase_set);
        mEtPhraseCount = (EditText) findViewById(R.id.round_etxt_phrase_count);
        mSwLowercase = (Switch) findViewById(R.id.round_sw_lowercase);
        mSwAlphaNum = (Switch) findViewById(R.id.round_sw_alphanumeric);
        mSwAutoCapFirst = (Switch) findViewById(R.id.round_sw_auto_cap_first);
        mTvCurKeyboard = (TextView) findViewById(R.id.round_txt_cur_keyboard);
        mBtnKeyboard = (Button) findViewById(R.id.round_btn_keyboard);
        mSwSuggestions = (Switch) findViewById(R.id.round_sw_suggestions);
        mSwNextBtn = (Switch) findViewById(R.id.round_sw_next_button);
        mSwTraining = (Switch) findViewById(R.id.round_sw_training);
        mBtnStart = (Button) findViewById(R.id.round_btn_start);

        // More or less just for Debug. Starts Session if it hasn't started yet.
        if (!TeaLib.getInstance().isSessionStarted()) {
            TeaLib.getInstance().startSessionWithDefaultName(getApplicationContext());
        }

        initTvs();
        initSpinner();
        initBtns();

        // Position cursor to the right
        setCursorToTheEnd(mEtPhraseCount);

        // Hide Keyboard
        KeyBoardHelper.hideKeyboard(this);
    }

    private void initTvs() {
        mTvSessionName.setText(String.format(getString(R.string.session_name), TeaLib.getInstance().getSessionName()));
        updateCurKeyboard();
    }

    private void updateCurKeyboard() {
        mTvCurKeyboard.setText(TeaLib.getInstance().getKeyboardName(getApplicationContext()));
    }

    private void initSpinner() {
        List<String> spinnerArray;
        try {
            spinnerArray = TeaLib.getInstance().getAssetsPhrasesetsFileList(this);
        } catch (IOException e) {
            Toast.makeText(this, "ERROR: Unable to read 'assets/phrasesets' directory",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
            return;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpPhraseSet.setAdapter(adapter);
    }


    private void initBtns() {
        mBtnKeyboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TeaLib.showInputMethodPicker(RoundActivity.this.getApplicationContext());
                mTvCurKeyboard.setText(getString(R.string.click_to_refresh));
            }
        });
        mBtnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int phraseCount = getPhraseCount();
                if (phraseCount == -1)
                    return;

                int roundNr = -1;
                try {
                    roundNr = TeaLib.getInstance().prepareNewRound(
                            new RoundSettings(
                                    mSpPhraseSet.getSelectedItem().toString(),
                                    phraseCount,
                                    mSwLowercase.isChecked(),
                                    mSwAlphaNum.isChecked(),
                                    mSwAutoCapFirst.isChecked(),
                                    mSwSuggestions.isChecked(),
                                    mSwNextBtn.isChecked(),
                                    mSwTraining.isChecked()),
                            RoundActivity.this);
                } catch (IOException e) {
                    Toast.makeText(getApplicationContext(), getString(R.string.could_not_open_phraseset),
                            Toast.LENGTH_LONG).show();
                }
                LinkingHelper.goToMainActivity(RoundActivity.this, roundNr, mSwNextBtn.isChecked());
            }
        });

        mTvCurKeyboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateCurKeyboard();
            }
        });

        mSwTraining.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    mEtPhraseCount.setText(mTrainingPhraseCount);
                    return;
                }
                mEtPhraseCount.setText(mDefaultPhraseCount);
            }
        });
    }

    private int getPhraseCount() {
        Integer nr;
        try {
            nr = Integer.parseInt(mEtPhraseCount.getText().toString());
        } catch (NumberFormatException e) {
            mEtPhraseCount.setError(getString(R.string.please_enter_a_valid_nr));
            // Position cursor to the right
            setCursorToTheEnd(mEtPhraseCount);
            return -1;
        }
        Log.d(LOGTAG, "txt=" + mEtPhraseCount.getText().toString() + " || nr=" + nr);
        if (nr.equals(0)) {
            mEtPhraseCount.setError(getString(R.string.please_enter_a_valid_nr));
            // Position cursor to the right
            setCursorToTheEnd(mEtPhraseCount);
            return -1;
        }
        return nr;
    }

    @Override
    public void onBackPressed() {
        LinkingHelper.goToSessionActivity(this);
    }

    private void setCursorToTheEnd(EditText etx) {
        etx.setSelection(etx.getText().length(), etx.getText().length());
    }

    public void onPresetSelected(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        int viewId = view.getId();

        if (checked) {
            setPresets(viewId);
        }
    }

    public void setPresets(int viewId) {
        // PhraseSet
        mSpPhraseSet.setSelection(0);   // CHI 2003
        // PhraseCount
        mEtPhraseCount.setText(mDefaultPhraseCount); // 10 TODO
        mSwLowercase.setChecked(false);
        mSwAlphaNum.setChecked(true);
        mSwAutoCapFirst.setChecked(false);  // only needed for voice input, b/c autocap first letter cannot be prevented
        String keyboardCode = "";
        String keyboard = "";
        // Check which radio button was clicked
        switch (viewId) {
            case R.id.round_rb_1:
                mSwSuggestions.setChecked(false);
                mSwNextBtn.setChecked(false);
                keyboardCode = TeaLib.ANDROID_DEFAULT_KEYBOARD;
                keyboard = TeaLib.ANDROID_DEFAULT_KEYBOARD + " (en_us)";
                break;
            case R.id.round_rb_2:
                mSwSuggestions.setChecked(true);
                mSwNextBtn.setChecked(false);
                keyboardCode = TeaLib.ANDROID_DEFAULT_KEYBOARD;
                keyboard = TeaLib.ANDROID_DEFAULT_KEYBOARD + " (en_us)";
                break;
            case R.id.round_rb_3:
                mSwSuggestions.setChecked(true);
                mSwNextBtn.setChecked(false);
                keyboardCode = TeaLib.ANDROID_DEFAULT_KEYBOARD;
                keyboard = TeaLib.ANDROID_DEFAULT_KEYBOARD + " (en_us)";
                break;
            case R.id.round_rb_4:
                mSwSuggestions.setChecked(true);
                mSwNextBtn.setChecked(false);
                keyboardCode = TeaLib.SWIFTKEY_KEYBOARD;
                keyboard = TeaLib.SWIFTKEY_KEYBOARD;
                break;
            case R.id.round_rb_5:
                mSwSuggestions.setChecked(true);
                mSwNextBtn.setChecked(false);
                keyboardCode = TeaLib.SWIFTKEY_KEYBOARD;
                keyboard = TeaLib.SWIFTKEY_KEYBOARD;
                break;
            case R.id.round_rb_6:
                mSwSuggestions.setChecked(true);
                mSwNextBtn.setChecked(false);
                keyboardCode = TeaLib.SWYPE_KEYBOARD;
                keyboard = TeaLib.SWYPE_KEYBOARD;
                break;
            case R.id.round_rb_7:
                mSwSuggestions.setChecked(true);
                mSwNextBtn.setChecked(false);
                keyboardCode = TeaLib.SWYPE_KEYBOARD;
                keyboard = TeaLib.SWYPE_KEYBOARD;
                break;
            case R.id.round_rb_8:
                mSwSuggestions.setChecked(false);
                mSwNextBtn.setChecked(true);
                mSwAutoCapFirst.setChecked(true);
                keyboardCode = TeaLib.ANDROID_DEFAULT_KEYBOARD;
                keyboard = TeaLib.ANDROID_DEFAULT_KEYBOARD + " " + getString(R.string.on_next_screen);
                break;
        }

        if(!TeaLib.getInstance().getKeyboardName(this).contains(keyboardCode)) {
            Toast.makeText(this, String.format(getString(R.string.change_keyboard_to), keyboard), Toast.LENGTH_SHORT)
                    .show();
        }
        mSvMain.smoothScrollBy(0, mSvMain.getBottom());
    }
}
