package com.szoldapps.tealib.model;

/**
 * Created by thomasszoldatits on 24.02.15.
 */
public class RoundSettings {

    private final String phraseSetName;
    private final int phraseCount;
    private String keyboard;
    private final boolean lowerCaseOnly;
    private final boolean alphaNumOnly;
    private final boolean autoCapFirst;
    private final boolean showSuggestions;
    private final boolean showNextBtn;
    private final boolean trainingRound;

    public RoundSettings(String phraseSetName, int phraseCount, boolean lowerCaseOnly, boolean alphaNumOnly, boolean
            autoCapFirst, boolean showSuggestions, boolean showNextBtn, boolean trainingRound) {
        this.phraseSetName = phraseSetName;
        this.phraseCount = phraseCount;
        this.lowerCaseOnly = lowerCaseOnly;
        this.alphaNumOnly = alphaNumOnly;
        this.autoCapFirst = autoCapFirst;
        this.showSuggestions = showSuggestions;
        this.showNextBtn = showNextBtn;
        this.trainingRound = trainingRound;
    }

    public String getPhraseSetName() {
        return phraseSetName;
    }

    public int getPhraseCount() {
        return phraseCount;
    }

    public boolean isLowerCaseOnly() {
        return lowerCaseOnly;
    }
    public boolean isAlphaNumOnly() {
        return alphaNumOnly;
    }

    public String getKeyboard() {
        return keyboard;

    }

    public void setKeyboard(String keyboard) {
        this.keyboard = keyboard;
    }

    public boolean isShowSuggestions() {
        return showSuggestions;
    }

    public boolean isTrainingRound() {
        return trainingRound;
    }

    public boolean isShowNextBtn() {
        return showNextBtn;
    }

    public boolean isAutoCapFirst() {
        return autoCapFirst;
    }
}
