package com.szoldapps.tealib.stats;

import com.szoldapps.tealib.model.Keystroke;

import java.util.List;

/**
 * Created by thomasszoldatits on 24.02.15.
 */
public class PhraseStats {

    private String presented;
    private String transcribed;
    private Metrics metrics;
    private List<Keystroke> keystrokes;
    private long startTime;
    private long endTime;

    public PhraseStats(String presented, String transcribed, List<Keystroke> keystrokes) {
        this.presented = presented;
        this.transcribed = transcribed;
        this.keystrokes = keystrokes;
    }

    public String getPresented() {
        return presented;
    }

    public void setPresented(String presented) {
        this.presented = presented;
    }

    public String getTranscribed() {
        return transcribed;
    }

    public void setTranscribed(String transcribed) {
        this.transcribed = transcribed;
    }

    public Metrics getMetrics() {
        return metrics;
    }

    public void setMetrics(Metrics metrics) {
        this.metrics = metrics;
    }

    public List<Keystroke> getKeystrokes() {
        return keystrokes;
    }

    public void setKeystrokes(List<Keystroke> keystrokes) {
        this.keystrokes = keystrokes;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }
}
