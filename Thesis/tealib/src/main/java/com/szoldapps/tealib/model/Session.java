package com.szoldapps.tealib.model;

import android.content.Context;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thomasszoldatits on 16.02.15.
 */
public class Session {

    private String sessionName;
    private String deviceName;
    private List<Round> rounds;
    private int roundCounter = 0;

    public Session(String sessionName, String deviceName) {
        this.sessionName = sessionName;
        this.deviceName = deviceName;
        this.rounds = new ArrayList<Round>();
    }

    public int prepareNewRound(RoundSettings roundSettings, Context context) throws IOException {
        rounds.add(new Round(roundCounter++, roundSettings, context));
        return roundCounter - 1;
    }

    public String getSessionName() {
        return sessionName;
    }

    public Round getRound(int roundNr) {
        return rounds.get(roundNr);
    }

    public List<Round> getRounds() {
        return rounds;
    }

    public void deleteLastRound() {
        rounds.remove(rounds.size() - 1);
        roundCounter--;
    }

    public String getDeviceName() {
        return deviceName;
    }
}
