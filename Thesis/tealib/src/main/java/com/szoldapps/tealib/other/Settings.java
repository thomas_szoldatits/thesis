package com.szoldapps.tealib.other;

import android.util.Log;

public class Settings {

    private static final String LOGTAG = Settings.class.getSimpleName();

    public static String SEP = ";";
    public static String DECIMAL_FORMAT = "#.####";
    public static String DECIMAL_FORMAT_FOR_STATS_ACTIVITY = "0.00";
    public static String KEYSTROKES_LOG_FILENAME = "KeystrokesLog.csv";
    public static String SPACE_REP = "<SP>";
    public static String BACKSPACE_REP = "<BS>";

    public static void resetToDefault() {
        Log.d(LOGTAG, "---------- RESET SETTINGS TO DEFAULT ----------");
        SEP = ";";
        DECIMAL_FORMAT = "###.##";
        KEYSTROKES_LOG_FILENAME = "KeystrokesLog.csv";
        SPACE_REP = "<SP>";
        BACKSPACE_REP = "<BS>";
    }

    public static String getSettingsDescription() {
        String ret = "";
        ret += "\nCurrent Settings:\n";
        ret += "Separator: '" + SEP + "'\n";
        return ret;
    }

    public static String getStringRepresentationForChar(char c) {
        String ret = "";
        switch (c) {
            case ' ':
                ret += SPACE_REP;
                break;
            case 8:
                ret += BACKSPACE_REP;
                break;
            default:
                ret += c;
        }
        return ret;
    }

    public static boolean isBackspace(char c) {
        return c == 8;
    }


}
