package com.szoldapps.tealib.csv;


import android.os.Environment;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Creates or updates {@link com.szoldapps.tealib.csv.OutputFile} in Downloads directory.
 * It only calls {@link OutputFile#getContentRows()} in a loop and writes it to a file named {@link
 * OutputFile#getFileName()} in the Downloads directory.
 */
public class CSVWriter {

    private static final String LOGTAG = CSVWriter.class.getSimpleName();

    /**
     * Creates or updates {@link com.szoldapps.tealib.csv.OutputFile} in Downloads directory.
     * It only calls {@link OutputFile#getContentRows()} in a loop and writes it to a file named {@link
     * OutputFile#getFileName()} in the Downloads directory.
     *
     * @param outputFile    file to write
     * @throws IOException  if file cannot be created or updated
     */
    public static void writeToCSV(OutputFile outputFile) throws IOException {

        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File f = new File(path, "/" + outputFile.getFileName());

        FileWriter fw;
        BufferedWriter out;
        if (f.exists()) {
            fw = new FileWriter(f, false);
            out = new BufferedWriter(fw);
            Log.d(LOGTAG, "File UPDATED: " + f.getAbsolutePath());
        } else {
            fw = new FileWriter(f.getAbsolutePath());
            out = new BufferedWriter(fw);
            Log.d(LOGTAG, "File CREATED: " + f.getAbsolutePath());
        }

        for (String row : outputFile.getContentRows()) {
            out.write(row);
            out.newLine();
        }

        out.close();
    }

}
