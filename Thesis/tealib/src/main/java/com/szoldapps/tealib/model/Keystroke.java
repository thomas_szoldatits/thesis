package com.szoldapps.tealib.model;

import com.szoldapps.tealib.other.Settings;

/**
 * Created by thomasszoldatits on 16.02.15.
 */
public class Keystroke {

    private final char cHar;
    private final long time;
    private boolean inT;
    private boolean autoAdded;

    public Keystroke(long time, char c, boolean autoAdded) {
        this.cHar = c;
        this.time = time;
        if (Settings.isBackspace(c)) {
            setInT(false);
        } else {
            setInT(true);
        }
        this.autoAdded = autoAdded;
    }

    public char getChar() {
        return cHar;
    }

    public long getTime() {
        return time;
    }

    public boolean isInT() {
        return inT;
    }

    public void setInT(boolean b) {
        this.inT = b;
    }

    public boolean isAutoAdded() {
        return autoAdded;
    }

    public void setAutoAdded(boolean mAutoAdded) {
        this.autoAdded = mAutoAdded;
    }
}
