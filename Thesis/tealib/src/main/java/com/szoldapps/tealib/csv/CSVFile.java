package com.szoldapps.tealib.csv;

import android.content.Context;

import com.szoldapps.tealib.model.Keystroke;
import com.szoldapps.tealib.other.DateHelper;
import com.szoldapps.tealib.other.Settings;
import com.szoldapps.tealib.stats.Metrics;
import com.szoldapps.tealib.stats.PhraseStats;
import com.szoldapps.tealib.stats.RoundStats;
import com.szoldapps.tealib.stats.SessionStats;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CSVFile implements OutputFile {

    private static final String LOGTAG = CSVFile.class.getSimpleName();
    private final Context mContext;
    private final SessionStats mSessionStats;
    private final DecimalFormat df = new DecimalFormat(Settings.DECIMAL_FORMAT);
    private final int mSepCount = 21; // columnCount-1

    public CSVFile(SessionStats sessionStats, Context context) {
        this.mSessionStats = sessionStats;
        this.mContext = context;
    }

    @Override
    public List<String> getContentRows() {
        List<String> stringRowList = new ArrayList<String>();
        stringRowList.addAll(getSessionLogRows());
        stringRowList.addAll(getRoundLogRows());
        stringRowList.addAll(getKeyLogRows());
        return stringRowList;
    }

    @Override
    public String getFileName() {
        return mSessionStats.getName() + "_" +
                DateHelper.getDateStr(
                        mSessionStats.getStartTime(), DateHelper.DateHelperFormat.DATE_FOR_TEALIB_CSV, mContext) +
                ".csv";
    }

    private List<String> getSessionLogRows() {
        List<String> rows = new ArrayList<String>();

        // Session Stats
        rows.add("Session Name" + getSEP(1) + mSessionStats.getName() + getSEP(mSepCount - 1));
        rows.add("Device" + getSEP(1) + mSessionStats.getDevice() + getSEP(mSepCount - 1));
        rows.add("Screen" + getSEP(1) + mSessionStats.getScreenSize() + getSEP(mSepCount - 1));

        rows.add(getSEP(mSepCount));  // just for some separation

        // SessionStats Log opened
        rows.add("[Session] Log opened: " +
                DateHelper.getDateStr(mSessionStats.getStartTime(), DateHelper.DateHelperFormat.MEDIUM_DATE_AND_TIME_WITH_WEEKDAY, mContext) +
                getSEP(mSepCount));
        rows.add("Round" + getSEP(1) + "Keyboard" + getSEP(1) + getMetricsHeadlineRow());
        for (RoundStats rs : mSessionStats.getRoundStats()) {
            // Add RowMetrics
            rows.add("Round " + rs.getNr() + getSEP(1) +
                    rs.getKeyboard() + getSEP(1) +
                    getMetricsRow(rs.getMetrics()));
        }

        // add SessionMetrics (values)
        rows.add("[SUMS]" + getSEP(2) + getMetricsRow(mSessionStats.getMetrics()));
        // SessionStats Log closed
        rows.add("[Session] Log closed: " +
                DateHelper.getDateStr(mSessionStats.getEndTime(),
                        DateHelper.DateHelperFormat.MEDIUM_DATE_AND_TIME_WITH_WEEKDAY,
                        mContext) +
                getSEP(mSepCount));

        rows.add(getSEP(mSepCount));  // just for some separation
        return rows;
    }

    private List<String> getRoundLogRows() {
        List<String> stringRowList = new ArrayList<String>();
        for (RoundStats rs : mSessionStats.getRoundStats()) {
            // Round Opening Row
            stringRowList.add("[Round " + rs.getNr() + "] Log opened: " +
                    DateHelper.getDateStr(rs.getStartTime(),
                            DateHelper.DateHelperFormat.MEDIUM_DATE_AND_TIME_WITH_WEEKDAY,
                            mContext) +
                    getSEP(mSepCount));

            // Round Settings
            stringRowList.add("Phrase Set" + getSEP(1) + rs.getPhraseSet() + getSEP(mSepCount - 1));
            stringRowList.add("Phrase Count" + getSEP(1) + rs.getPhraseCount() + getSEP(mSepCount - 1));
            stringRowList.add("Lowercase Only" + getSEP(1) + rs.isLowerCaseOnly() + getSEP(mSepCount - 1));
            stringRowList.add("Alphanumeric Only" + getSEP(1) + rs.isAlphaNumOnly() + getSEP(mSepCount - 1));
            stringRowList.add("Keyboard" + getSEP(1) + rs.getKeyboard() + getSEP(mSepCount - 1));
            stringRowList.add("Suggestions shown" + getSEP(1) + rs.isShowSuggestions() + getSEP(mSepCount - 1));
            stringRowList.add("Next Button shown" + getSEP(1) + rs.isShowNextBtn() + getSEP(mSepCount - 1));

            // Round Data
            stringRowList.add("Presented (P)" + getSEP(1) + "Transcribed (T)" + getSEP(1) + getMetricsHeadlineRow());
            for (PhraseStats ps : rs.getPhraseStats()) {
                stringRowList.add(removeSemicolonsAndReturns(ps.getPresented()) +
                        getSEP(1) + removeSemicolonsAndReturns(ps.getTranscribed()) +
                        getSEP(1) + getMetricsRow(ps.getMetrics()));
            }
            stringRowList.add("[SUMS]" + getSEP(2) + getMetricsRow(rs.getMetrics()));

            // Round Closing Row
            stringRowList.add("[Round " + rs.getNr() + "] Log closed: " +
                    DateHelper.getDateStr(rs.getEndTime(),
                            DateHelper.DateHelperFormat.MEDIUM_DATE_AND_TIME_WITH_WEEKDAY,
                            mContext) +
                    getSEP(mSepCount));

            // just for some separation
            stringRowList.add(getSEP(mSepCount));
        }
        return stringRowList;
    }

    private String removeSemicolonsAndReturns(String str) {
        str = str.replace(";", "");
        return str.replace("\n", "");
    }

    private List<String> getKeyLogRows() {
        List<String> stringRowList = new ArrayList<String>();
        // Keylog part
        int pCounter = 1;
        for (RoundStats rs : mSessionStats.getRoundStats()) {
            for (PhraseStats ps : rs.getPhraseStats()) {
                stringRowList.add("[R" + rs.getNr() + ": Phrase " + pCounter + "] " + ps.getPresented() +
                        getSEP(mSepCount));
                stringRowList.add("time" + getSEP(1) + "char" + getSEP(1) + "in T" + getSEP(1) + "autoAdded" +
                        getSEP(mSepCount - 3));
                for (Keystroke k : ps.getKeystrokes()) {
                    stringRowList.add(k.getTime() +
                            getSEP(1) +
                            removeSemicolonsAndReturns(Settings.getStringRepresentationForChar(k.getChar())) +
                            getSEP(1) +
                            k.isInT() +
                            getSEP(1) +
                            k.isAutoAdded() +
                            getSEP(mSepCount - 3));
                }
                stringRowList.add("[R" + rs.getNr() + ": Phrase " + pCounter++ + "] " + ps.getTranscribed() +
                        getSEP(mSepCount));
            }
        }
        return stringRowList;
    }

    private String getMetricsHeadlineRow() {
        return "P chars" +
                getSEP(1) + "T chars" +
                getSEP(1) + "Input Time (in s)" +
                getSEP(1) + "Pause Time (in s)" +
                getSEP(1) + "Total Time (in s)" +
                getSEP(1) + "WPM" +
                getSEP(1) + "MSD" +
                getSEP(1) + "BS count" +
                getSEP(1) + "KSPC" +
                getSEP(1) + "C" +
                getSEP(1) + "INF" +
                getSEP(1) + "F" +
                getSEP(1) + "IF" +
                getSEP(1) + "CER (%)" +
                getSEP(1) + "UER (%)" +
                getSEP(1) + "TER (%)" +
                getSEP(1) + "CE (%)" +
                getSEP(1) + "PC (%)" +
                getSEP(1) + "UB (%)" +
                getSEP(1) + "WB (%)";
    }

    private String getMetricsRow(Metrics m) {
        return m.getpChars() +
                getSEP(1) + m.gettChars() +
                getSEP(1) + toDec(m.getInputTime()) +
                getSEP(1) + toDec(m.getPauseTime()) +
                getSEP(1) + toDec(m.getTotalTime()) +
                getSEP(1) + toDec(m.getWpm()) +
                getSEP(1) + m.getMsd() +
                getSEP(1) + m.getBsCount() +
                getSEP(1) + toDec(m.getKspc()) +
                getSEP(1) + m.getC() +
                getSEP(1) + m.getInf() +
                getSEP(1) + m.getF() +
                getSEP(1) + m.getIf() +
                getSEP(1) + toDec(m.getCer()) +
                getSEP(1) + toDec(m.getUer()) +
                getSEP(1) + toDec(m.getTer()) +
                getSEP(1) + toDec(m.getCorrEff()) +
                getSEP(1) + toDec(m.getPartCon()) +
                getSEP(1) + toDec(m.getUtilBandwidth()) +
                getSEP(1) + toDec(m.getWastedBandwidth());
    }

    private String toDec(double d) {
        return df.format(d);
    }

    private String getSEP(int count) {
        String ret = "";
        for (int i = 0; i < count; i++) {
            ret += Settings.SEP;
        }
        return ret;
    }


}
