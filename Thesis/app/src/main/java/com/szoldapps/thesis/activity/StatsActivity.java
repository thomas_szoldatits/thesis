package com.szoldapps.thesis.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.dropbox.sync.android.DbxAccountManager;
import com.dropbox.sync.android.DbxException;
import com.dropbox.sync.android.DbxFile;
import com.dropbox.sync.android.DbxFileStatus;
import com.dropbox.sync.android.DbxFileSystem;
import com.dropbox.sync.android.DbxPath;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.szoldapps.tealib.TeaLib;
import com.szoldapps.tealib.csv.CSVFile;
import com.szoldapps.tealib.stats.SessionStats;
import com.szoldapps.thesis.helper.LinkingHelper;
import com.szoldapps.thesis.adapter.StatsListViewAdapter;
import com.szoldapps.thesis.R;

import java.io.IOException;


public class StatsActivity extends ActionBarActivity {

    private static final String LOGTAG = StatsActivity.class.getSimpleName();
    private ListView mLv;
    private SessionStats mSessionStats;
    private FloatingActionsMenu mFamMenu;
    private FloatingActionButton mBtnUpload;
    private FloatingActionButton mBtnNewRoundSame;
    private FloatingActionButton mBtnNewRoundChange;
    private FloatingActionButton mBtnNewSession;
    private ProgressDialog mProgBarDiag;
    // Dropbox
    private static final String APP_KEY = "h7zqhwase7mccyg";
    private static final String APP_SECRET = "2pkgv4by8rzpuld";
    private DbxAccountManager mDbxAcctMgr;
    private static final int REQUEST_LINK_TO_DBX = 0;  // Random Value (This value is up to you)
    private boolean mShowNextBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats);

        mLv = (ListView) findViewById(R.id.lvCustomList);

        mShowNextBtn = getIntent().getBooleanExtra(MainActivity.EXTRA_SHOW_NEXT_BTN, false);

        getSessionStats();

        if(mSessionStats != null) {
            // Init Adapter
            StatsListViewAdapter lvAdapter = new StatsListViewAdapter(mSessionStats, getApplicationContext());
            // Add Header to LV
            mLv.addHeaderView(lvAdapter.getHeader());
            // Set Adapter
            mLv.setAdapter(lvAdapter);

            mDbxAcctMgr = DbxAccountManager.getInstance(getApplicationContext(), APP_KEY, APP_SECRET);
        } else {
            LinkingHelper.goToRoundActivity(this);
        }
        // Init Buttons
        initButtons();

    }

    private void initButtons() {
        mFamMenu = (FloatingActionsMenu) findViewById(R.id.stats_fam_menu);
        mBtnUpload = (FloatingActionButton) findViewById(R.id.stats_btn_upload);
        mBtnNewRoundSame = (FloatingActionButton) findViewById(R.id.stats_btn_new_round_same);
        mBtnNewRoundChange = (FloatingActionButton) findViewById(R.id.stats_btn_new_round_change);
        mBtnNewSession = (FloatingActionButton) findViewById(R.id.stats_btn_new_session);

        mBtnNewRoundSame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    LinkingHelper.goToMainActivity(StatsActivity.this,
                            TeaLib.getInstance().prepareNewRoundWithSameSettings(StatsActivity.this), mShowNextBtn);
                } catch (IOException e) {
                    // TODO: Handle Error
                    e.printStackTrace();
                }
            }
        });

        mBtnNewRoundChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinkingHelper.goToRoundActivity(StatsActivity.this);
            }
        });

        mBtnNewSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LinkingHelper.goToSessionActivity(StatsActivity.this);
            }
        });

        mBtnUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mDbxAcctMgr.hasLinkedAccount()) {
                    onClickLinkToDropbox();
                } else {
                    uploadSessionDataToDropbox();
                }
            }
        });
    }

    private void getSessionStats() {
        // DEBUG -----------------
//        TeaLib.getInstance().startSession("Thomas");
//        TeaLib.getInstance().fillSessionWithDummyData(this);
        // DEBUG -----------------
        mSessionStats = TeaLib.getInstance().getSessionStats(this);
        try {
            TeaLib.getInstance().writeCSVFileToDownloadDir(mSessionStats, this.getApplicationContext());
            Toast.makeText(getApplicationContext(), getString(R.string.csv_file_created_in_downloads),
                    Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(), getString(R.string.error_csv_file_creation),
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        Log.i(LOGTAG, "Back button pressed, NO ACTION!");
    }


    private void onClickLinkToDropbox() {
        mDbxAcctMgr.startLink(this, REQUEST_LINK_TO_DBX);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_LINK_TO_DBX) {
            if (resultCode == Activity.RESULT_OK) {
                // ... Start using Dropbox files.
                Toast.makeText(this, getString(R.string.dropbox_link_success), Toast.LENGTH_LONG).show();
                uploadSessionDataToDropbox();
            } else {
                // ... Link failed or was cancelled by the user.
                Toast.makeText(this, getString(R.string.dropbox_link_error), Toast.LENGTH_LONG).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    /**
     * Gets CSVFile from TeaLib and uploads it to DB.
     * Moreover calls launchBarDialog();
     */
    private void uploadSessionDataToDropbox() {
        mFamMenu.collapse();
        try {
            CSVFile csv = TeaLib.getInstance().getCSVFile(mSessionStats, getApplicationContext());
            DbxFileSystem dbxFs = DbxFileSystem.forAccount(mDbxAcctMgr.getLinkedAccount());
            launchBarDialog(csv.getFileName());

            if (!hasNetworkConnection()) {
                toastNoInternetConnection();
            }

            DbxFile dbxFile;
            try {
                // Tries to create File (there is no way to check whether file already exists)
                dbxFile = dbxFs.create(new DbxPath(csv.getFileName()));
            } catch (DbxException e) {
                // Opens it if File already exists
                dbxFile = dbxFs.open(new DbxPath(csv.getFileName()));
            }

            String out = "";
            for (String row : csv.getContentRows()) {
                out += row + "\n";
            }
            dbxFile.writeString(out);
            dbxFile.addListener(new DbxFile.Listener() {

                @Override
                public void onFileChange(DbxFile file) {
                    // Check testFile.getSyncStatus() and read if it's ready
                    try {
                        DbxFileStatus stat = file.getSyncStatus();
//                        Log.e(LOGTAG, stat.bytesTransferred + "/" + stat.bytesTotal);
                        if (stat.bytesTransferred == stat.bytesTotal) {
                            file.close();
                            mProgBarDiag.dismiss();
                            Toast.makeText(StatsActivity.this, "File successfully uploaded!",
                                    Toast.LENGTH_SHORT).show();
                        }
                    } catch (DbxException e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (DbxException.Unauthorized unauthorized) {
            Log.e(LOGTAG, "DbxException.Unauthorized");
            unauthorized.printStackTrace();
        } catch (IOException e) {
            toastNoInternetConnection();
        }
    }

    private void toastNoInternetConnection() {
        Log.e(LOGTAG, "--\n--\n--IOException1--\n--\n--");
        mProgBarDiag.dismiss();
        Toast.makeText(StatsActivity.this, "NO Internet Connection! Connect and retry!", Toast.LENGTH_SHORT).show();
    }

    /**
     * Opens ProgressBarDialog and shows 'loading' Progressbar to signal that file is being uploaded to DB
     *
     * @param fileName Name of the file that's being uploaded
     */
    private void launchBarDialog(String fileName) {
        mProgBarDiag = new ProgressDialog(this);
        mProgBarDiag.setTitle("Uploading to Dropbox ...");
        mProgBarDiag.setMessage("'" + fileName + "'\nis being uploaded ...");
        mProgBarDiag.setProgressStyle(mProgBarDiag.STYLE_HORIZONTAL);
        mProgBarDiag.setIndeterminate(true);
        mProgBarDiag.setProgressNumberFormat(null);
        mProgBarDiag.setProgressPercentFormat(null);
        mProgBarDiag.show();

    }

    private boolean hasNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;

    }

}