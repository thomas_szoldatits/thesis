package com.szoldapps.thesis.activity;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.szoldapps.tealib.TeaLib;
import com.szoldapps.tealib.listener.TeaLibListener;
import com.szoldapps.tealib.widget.TeaEditText;
import com.szoldapps.thesis.helper.KeyBoardHelper;
import com.szoldapps.thesis.helper.LinkingHelper;
import com.szoldapps.thesis.R;


public class MainActivity extends ActionBarActivity {

    private static final String LOGTAG = MainActivity.class.getSimpleName();
    public static final String EXTRA_ROUND_NR = LOGTAG + ".round_nr";
    public static final String EXTRA_SHOW_NEXT_BTN = LOGTAG + ".show_next_btn";

    private TextView mTvPresentedCount;
    private EditText mEtPresented;
    private TeaEditText mEtTranscribed;
    private TextView mTvTimer;
    private int mRoundNr;
    private ImageView mIvPausePlay;
    private Button mBtnNext;
    private boolean mShowNextBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mTvPresentedCount = (TextView) findViewById(R.id.txt_presented_count);
        mEtPresented = (EditText) findViewById(R.id.etxt_presented);
        mEtTranscribed = (TeaEditText) findViewById(R.id.teaEt_transcribed);
        mTvTimer = (TextView) findViewById(R.id.txt_timer);
        mIvPausePlay = (ImageView) findViewById(R.id.main_iv_pause_play);
        mBtnNext = (Button) findViewById(R.id.main_btn_next);

        mRoundNr = getIntent().getIntExtra(EXTRA_ROUND_NR, -1); // DEBUG -1);
        if (mRoundNr == -1) {
            // TODO: Show Error
            return;
        }

        mShowNextBtn = getIntent().getBooleanExtra(EXTRA_SHOW_NEXT_BTN, false);
        if (mShowNextBtn) {
            mBtnNext.setVisibility(View.VISIBLE);
        } else {
            mBtnNext.setVisibility(View.GONE);
        }

        // DEBUG -----------------
//        TeaLib.getInstance().startSession("Thomas");
//        try {
//            mRoundNr = TeaLib.getInstance().prepareNewRound("CHI 2003 (1)", 3, this);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        // DEBUG -----------------

        TeaLib.getInstance().startRound(
                mRoundNr,
                mEtPresented,
                mEtTranscribed,
                mTvTimer,
                new TeaLibListener() {
                    @Override
                    public void onPlayEvent() {
                        mIvPausePlay.setImageDrawable(getResources().getDrawable(R.drawable.ic_pause));
                    }

                    @Override
                    public void onPauseEvent() {
                        mIvPausePlay.setImageDrawable(getResources().getDrawable(R.drawable.ic_play));
                    }

                    @Override
                    public void noMorePhrasesLeft() {
                        LinkingHelper.goToStatsActivity(MainActivity.this, mShowNextBtn);
                    }

                    @Override
                    public void trainingIsOver() {
                        LinkingHelper.goToRoundActivity(MainActivity.this);
                    }

                    @Override
                    public void onNextPhraseEvent(int phraseCounter, int phraseCount) {
                        mTvPresentedCount.setText(String.format(getString(R.string.presented_count), phraseCounter, phraseCount));
                    }
                },
                getApplicationContext());

        initButtons();
        mEtTranscribed.requestFocus();
        KeyBoardHelper.showKeyboard(this);

    }

    private void initButtons() {
        mIvPausePlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TeaLib.getInstance().toggleStopwatchPlayPause();
            }
        });
        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TeaLib.getInstance().goToNextPhrase();
            }
        });
    }

//    private void fillInfo() {
//        String currentKeyboard =  Settings.Secure.getString(getContentResolver(), Settings.Secure.DEFAULT_INPUT_METHOD);
//        String text = "Device = " + getDeviceName() + "\n";
//        text += "Keyboard = " + currentKeyboard;
//        mTxtInfo.setText(text);
//    }

    @Override
    protected void onPause() {
        super.onPause();
        if(!TeaLib.getInstance().isSessionPaused()) {
            TeaLib.getInstance().toggleStopwatchPlayPause();
        }
    }

    @Override
    public void onBackPressed() {
        Log.i(LOGTAG, "Back button pressed, NO ACTION!");
    }
}
