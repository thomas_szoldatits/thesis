package com.szoldapps.tealib.listener;

/**
 * Created by thomasszoldatits on 19.02.15.
 */
public interface TeaLibListener {

    public void onPlayEvent();

    public void onPauseEvent();

    public void noMorePhrasesLeft();

    public void trainingIsOver();

    public void onNextPhraseEvent(int phraseCounter, int phraseCount);

}
