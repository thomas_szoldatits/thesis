package com.szoldapps.tealib;

import com.szoldapps.tealib.model.Keystroke;
import com.szoldapps.tealib.other.Settings;

import java.util.List;

/**
 * Created by thomasszoldatits on 22.02.15.
 */
public class TeaMetrics {

    private static final String LOGTAG = TeaMetrics.class.getSimpleName();

    /**
     * (from http://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#Java)
     *
     * @param presented
     * @param transcribed
     * @return
     */
    public static int calcLevenshteinDistance(String presented, String transcribed) {

        int len0 = presented.length() + 1;
        int len1 = transcribed.length() + 1;

        // the array of distances
        int[] cost = new int[len0];
        int[] newCost = new int[len0];

        // initial cost of skipping prefix in String presented
        for (int i = 0; i < len0; i++) cost[i] = i;

        // dynamically computing the array of distances

        // transformation cost for each letter in transcribed
        for (int j = 1; j < len1; j++) {
            // initial cost of skipping prefix in String transcribed
            newCost[0] = j;

            // transformation cost for each letter in presented
            for (int i = 1; i < len0; i++) {
                // matching current letters in both strings
                int match = (presented.charAt(i - 1) == transcribed.charAt(j - 1)) ? 0 : 1;

                // computing cost for each transformation
                int cost_replace = cost[i - 1] + match;
                int cost_insert = cost[i] + 1;
                int cost_delete = newCost[i - 1] + 1;

                // keep minimum cost
                newCost[i] = Math.min(Math.min(cost_insert, cost_delete), cost_replace);
            }

            // swap cost/newCost arrays
            int[] swap = cost;
            cost = newCost;
            newCost = swap;
        }

        // the distance is the cost for transforming all letters in both strings
        return cost[len0 - 1];
    }

    /**
     * Calculates Words per minute (((|T|-1)/s)*60)/5
     *
     * @param transcribedLength
     * @param inputTimeInS
     * @return wpm (or 0 if wpm < 0 or if inputTimeInS <= 0.00)
     */
    public static double calcWordsPerMinute(int transcribedLength, double inputTimeInS) {
        if (inputTimeInS <= 0) {
            return 0;
        }
        double wpm = (((transcribedLength - 1) / inputTimeInS) * 60) / 5;
        return wpm < 0 ? 0 : wpm;
    }

    public static int getBackspaceCount(List<Keystroke> keystrokes) {
        int counter = 0;
        for (Keystroke k : keystrokes) {
            if (k.getChar() == 8) {
                counter++;
            }
        }
        return counter;
    }

    /**
     * Calculates Keystrokes per Character (|IS|/|T|)
     *
     * @param inputStreamLength
     * @param transcribedLength
     * @return KSPC (or 0 if KSPC < 0 or if transcribedLength <= 0)
     */
    public static double calcKeystrokesPerCharacter(int inputStreamLength, int transcribedLength) {
        if (transcribedLength <= 0) {
            return 0;
        }
        double kspc = inputStreamLength / (double) transcribedLength;
        return kspc < 0 ? 0 : kspc;
    }

    /**
     * Calculates Correct (C) Keystrokes -> C = max(|P|,|T|)-MSD(P,T)
     *
     * @param presented
     * @param transcribed
     * @return
     */
    public static int getCorrectKeystrokes(String presented, String transcribed) {
        return Math.max(presented.length(), transcribed.length()) - calcLevenshteinDistance(presented, transcribed);
    }

    /**
     * Calculates Incorrect Not Fixed Keystrokes (INF) -> INF = MSD(P,T)
     *
     * @param presented
     * @param transcribed
     * @return
     */
    public static int getIncorrectNotFixed(String presented, String transcribed) {
        return calcLevenshteinDistance(presented, transcribed);
    }

    /**
     * Counts the number of Fixes (F) / Backspaces in the Input Stream (IS)
     *
     * @param keystrokes
     * @return
     */
    public static int getFixes(List<Keystroke> keystrokes) {
        return getBackspaceCount(keystrokes);
    }

    /**
     * The IF keystrokes are those in the IS, but not in T and not in F
     *
     * @param keystrokes
     * @return
     */
    public static int getIncorrectFixed(List<Keystroke> keystrokes) {
        int counter = 0;
        for (Keystroke k : keystrokes) {
            if (Settings.isBackspace(k.getChar())) {
                continue;
            }
            if (!k.isInT()) {
                counter++;
            }
        }
        return counter;
    }

    /**
     * Corrected Error Rate -> CER = (IF / (C + INF + IF))
     *
     * @param c
     * @param iNF
     * @param iF
     * @return CER (in %)
     */
    public static double getCorrectedErrorRate(int c, int iNF, int iF) {
        return ((double) iF / (double) (c + iNF + iF));
    }

    /**
     * Uncorrected Error Rate -> UER = (INF / (C + INF + IF))
     *
     * @param c
     * @param iNF
     * @param iF
     * @return UER (in %)
     */
    public static double getUncorrectedErrorRate(int c, int iNF, int iF) {
        return ((double) iNF / (double) (c + iNF + iF));
    }

}
